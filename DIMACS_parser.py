class Var:
    def __init__(self, s):
        self.lit = int(s)
        self.sign = 1
        if self.lit < 0:
            self.lit = - self.lit
            self.sign = -1

    def toString(self):
        return str(self.sign * self.lit)

    def isTrueLit(self):
        return True if self.sign == 1 else False

    def isFalseLit(self):
        return True if self.sign == -1 else False

    def isTrue(self, o):
        return True if (o == 1 and self.isTrueLit()) or (o == 0 and self.isFalseLit()) else False

    def iFalse(self, o):
        return False if (o == 1 and self.isTrueLit()) or (o == 0 and self.isFalseLit()) else True


class String:
    def __init__(self, s):
        self.type = s[0] if s[0] in ["p", "c"] else "d"
        self.val = s

    def getP(self):
        s = self.val.split(" ")
        return int(s[2]), int(s[3])

    def rewriteP(self, vs, clauses):
        self.val = "p cnf %d %d\n" % (vs, clauses)
        return self.val

    def getC(self):
        if self.val.find("input variables") != -1:
            s = self.val.split(" ")
            return int(s[-1])
        return None

    def getV(self):
        v = self.val[:-3]
        s = v.split(" ")
        r = []
        for i in s:
            r.append(Var(i))
        return r

    def rewriteV(self, vs):
        self.val = ""
        for i in vs:
            self.val += "%s " % i.toString()
        self.val += "0\n"
        return self


class DIMACSParser:
    def __init__(self, fln):
        self.filename = "CNFs/%s" % fln
        self.c_strings = []
        self.d_strings = []
        self.p_string = ""
        self.useful_vars = []
        self.output_vars_number = None
        self.vars_to_add = []
        fl = open(self.filename)

        for i in fl:
            s = String(i)
            if s.type == "p":
                self.vars_number, self.clause_number = s.getP()
                self.useful_vars = [0] * (self.vars_number + 1)
                self.p_string = s
            elif s.type == "c":
                g = s.getC()
                if g:
                    self.input_vars_number = g
                self.c_strings.append(s)
            else:
                self.d_strings.append(s)
                v = s.getV()
                for j in v:
                    self.useful_vars[j.lit] = 1
        fl.close()

    def replaceInput(self, inputs, replace=""):
        self.replace(inputs, 1, self.input_vars_number)
        self.write(replace)

    def replaceOutput(self, outputs, replace=""):
        self.output_vars_number = len(outputs)
        #print len(outputs), self.vars_number, self.output_vars_number
        start = self.vars_number - self.output_vars_number + 1
        #print len(outputs), self.vars_number, self.output_vars_number, start
        self.replace(outputs, start, self.vars_number)
        #print "start", start
        self.write(replace)

    def write(self, replace=""):
        filename = replace if replace else self.filename
        w = open("CNFs/%s" % filename, "w")
        text = "%s%s%s" % (
                self.p_string.val,
               "".join(i.val for i in self.c_strings),
               "".join(i.val for i in self.d_strings)
        )
        w.write(text)
        w.close()

    def addSolution(self, solution):
        text = ""
        for i, v in enumerate(solution):
            if self.useful_vars[i + 1] == 1:
                if v == 1:
                    text += "%d " % -(i + 1)
                else:
                    text += "%d " % (i + 1)
        text += "0\n"
        s = String(text)
        self.d_strings.append(s)
        self.clause_number += 1
        self.p_string.rewriteP(self.vars_number, self.clause_number)
        self.write()

    def add_lines(self, vals, start):
        for i, v in enumerate(vals):
            if v != '-':
                s = (i + start) if v else -(i + start)
                vr = Var(s)
                st = String(" ").rewriteV([vr])
                self.d_strings.append(st)
        self.clause_number = len(self.d_strings)

    def replace(self, vals, start, end):
        #print "s", start, end, vals
        d_strings_new = []
        for i, v in enumerate(self.d_strings):
            s = v.getV()
            always_true = False
            varsnew = []
            for var in s:
                if start <= var.lit <= end and vals[var.lit - start] != "-":
                    if var.isTrue(vals[var.lit - start]):
                        always_true = True
                        break
                else:
                    varsnew.append(var)
                    #print var.lit
            if always_true or len(varsnew) == 0:
                self.clause_number -= 1
                continue
            else:
                v.rewriteV(varsnew)
                d_strings_new.append(v)


        self.d_strings = d_strings_new
        self.add_lines(vals, start)
        self.clause_number = len(self.d_strings)
        self.p_string.rewriteP(self.vars_number, self.clause_number)
