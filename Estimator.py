import copy
from fractions import Fraction
from sb_ import mean_amount
import random


SBOX = [0xc, 0x5, 0x6, 0xb, 0x9, 0x0, 0xa, 0xd, 0x3, 0xe, 0xf, 0x8, 0x4, 0x7, 0x1, 0x2]

def getComb(n):
    arr = [1, 1]
    for i in xrange(n - 1):
        arrt = [1]
        for j in xrange(len(arr) - 1):
            arrt.append(arr[j] + arr[j + 1])
        arrt.append(1)
        arr = arrt
    return arr

def getProb(o1, o2):
    omax = max(o1, o2)
    omin = min(o1, o2)
    den = pow(2, omin)
    p = pow(2, 4 - omax)
    s = Fraction((1.0 - 1.0 / den) ** p)
    arr = getComb(p)
    for i in xrange(1, p + 1):
        s += arr[i] * Fraction(i * ((1.0 - 1.0 / den) ** (p - i)) * ((1.0 / den) ** i))
    return (s)

class Sbox():
    def __init__(self, start_index, end_index):
        self.x = start_index
        self.y = end_index
        self.x_est = ["-"] * 4
        self.y_est = ["-"] * 4
        self.x_all_est = 0
        self.y_all_est = 0

    def printSbox(self):
        print self.x_est, self.y_est, self.x, self.y

    def calcEstimate(self, v):
        c = 1
        for i in v:
            c *= 2 if i == "-" else 1
        return c

    def calcEstimate_(self, v):
        c = 1
        for i in v:
            c *= 1 if i == "-" else 1
        return c

    def calcAllEstimates(self):
        self.x_all_est = self.calcEstimate(self.x_est)
        self.y_all_est = self.calcEstimate(self.y_est)
        return self.x_all_est, self.y_all_est

    def countZeroes(self, v):
        c = 0
        for i in v:
            if i == "-":
                c += 1
        return c

    def getRoot(self, set, v):
        c = self.countZeroes(set)
        try:
            return v ** (1.0 / c)
        except:
            return 1

    def notOnes(self, set, v):
        c = 0
        for i in set:
            if i != 1:
                c += 1
        return v ** (1.0 / c)

    def estimateSbox(self):
        self.calcAllEstimates()
        minest = min(self.x_all_est, self.y_all_est)

        dic = {}

        if self.x_all_est == minest and self.countZeroes(self.x_est) < self.countZeroes(self.y_est):
            set1 = self.x_est
            set1_ = self.x
            set2 = self.y_est
            set2_ = self.y
        else:
            set1 = self.y_est
            set1_ = self.y
            set2 = self.x_est
            set2_ = self.x

        v = getProb(4 - self.countZeroes(self.x_est), 4 - self.countZeroes(self.y_est))
        r = self.getRoot(set1, v)
        for i in xrange(4):
            if set1[i] == 0:
                set1[i] = r
                dic[set1_[i]] = r
        for i in xrange(4):
            if set2[i] == "-":  # TODO
                set2[i] = "d"  # r
                dic[set2_[i]] = "d"  # r

        self.calcAllEstimates()
        return dic

    def goodEstimateSbox(self):
        self.calcAllEstimates()
        dic = {}
        for i in xrange(4):
            if self.x_est[i] == 0:
                self.x_est[i] = 1
                dic[self.x[i]] = 1
        for i in xrange(4):
            if self.y_est[i] == 0:
                self.y_est[i] = 1
                dic[self.y[i]] = 1
        self.calcAllEstimates()
        self.calcAllEstimates()
        return dic

    def isGood(self):
        self.calcAllEstimates()
        if self.x_all_est < 16 and self.y_all_est < 16:
            return True
        return False

    def findVal(self, v, vl):
        dic = {}
        if v in self.x:
            self.x_est[self.x.index(v)] = vl
        if v in self.y:
            self.y_est[self.y.index(v)] = vl
        return dic

    def isDefined(self, mx=False):
        if self.countZeroes(self.x_est) == 0 or self.countZeroes(self.y_est) == 0:
            return True
        m = mean_amount(["".join(self.x_est), "".join(self.y_est)], SBOX)
        if m < 1 or (m==1 and not mx):
            return True

        return False

    def isPredicted(self):
        if (self.countZeroes(self.x_est) != 0 and self.countZeroes(self.y_est) != 0) and \
                (mean_amount(["".join(self.x_est), "".join(self.y_est)], SBOX) <= 1):
            return True
        return False

    def isDone(self):
        if self.countZeroes(self.x_est) == 0 and self.countZeroes(self.y_est) == 0:
            return True
        return False

    def getEntropyDelta(self):
        self.calcAllEstimates()

        return min(self.countZeroes(self.x_est), self.countZeroes(self.y_est)), \
               max(self.countZeroes(self.x_est), self.countZeroes(self.y_est))


class Config():
    def __init__(self, ciphername):
        if ciphername == "Present_80":
            self.master_key_length = 80
            self.key_length = 64
            self.sboxes_count = 1
            self.sboxes = [[0xc, 0x5, 0x6, 0xb, 0x9, 0x0, 0xa, 0xd, 0x3, 0xe, 0xf, 0x8, 0x4, 0x7, 0x1, 0x2]]
            self.rounds = 32
            self.shift = 19
        elif ciphername == "Present_128":
            self.master_key_length = 128
            self.key_length = 64
            self.sboxes_count = 2
            self.sboxes = [[0xc, 0x5, 0x6, 0xb, 0x9, 0x0, 0xa, 0xd, 0x3, 0xe, 0xf, 0x8, 0x4, 0x7, 0x1, 0x2],
                           [0xc, 0x5, 0x6, 0xb, 0x9, 0x0, 0xa, 0xd, 0x3, 0xe, 0xf, 0x8, 0x4, 0x7, 0x1, 0x2]]
            self.rounds = 32
            self.shift = 67
        elif ciphername == "MIBS_64":
            self.master_key_length = 64
            self.key_length = 32
            self.sboxes_count = 1
            self.sboxes = [[4, 0xF, 3, 8, 0xD, 0xA, 0xC, 0, 0xB, 5, 7, 0xE, 2, 6, 1, 9]]
            self.rounds = 32
            self.shift = 15
        elif ciphername == "MIBS_80":
            self.master_key_length = 80
            self.key_length = 32
            self.sboxes_count = 2
            self.sboxes = [[4, 0xF, 3, 8, 0xD, 0xA, 0xC, 0, 0xB, 5, 7, 0xE, 2, 6, 1, 9],
                           [4, 0xF, 3, 8, 0xD, 0xA, 0xC, 0, 0xB, 5, 7, 0xE, 2, 6, 1, 9]]
            self.rounds = 32
            self.shift = 19
        elif ciphername == "LBlock":
            self.master_key_length = 80
            self.key_length = 32
            self.sboxes_count = 2
            self.sboxes = [[8, 7, 0xE, 5, 0xF, 0xD, 0, 6, 0xB, 0xC, 9, 0xA, 2, 4, 1, 3],
                        [0xB, 5, 0xF, 0, 7, 2, 9, 0xD, 4, 8, 1, 0xC, 0xE, 0xA, 3, 6]]
            self.rounds = 32
            self.shift = 51


class Estimator():
    def __init__(self, variables, values, conf):
        self.conf = conf
        self.sboxes = []
        self.regstates = []
        self.values = ["-"] * (conf.master_key_length + conf.sboxes_count * 4 * (conf.rounds - 1))
        self.genRegVars()
        self.est_info = []
        vrs = copy.deepcopy(variables)
        for i in xrange(len(vrs)):
            v = variables[i]
            vd, vm = v / conf.key_length, v % conf.key_length
            v = (self.regstates[vd])[vm]
            vrs[i] = v
        self.fixVarsLight(vrs, values)

    def simpleEstimation(self):
        m = self.presentRegstates(False)
        self.fixVars([], [], False)
        n = self.presentRegstates(False)
        self.fixVars([], [], False)
        k = self.presentRegstates(False)
        return m, n, k

    def printSboxes(self):
        for s in self.sboxes:
            s.printSbox()
        print

    def not_fullnumbers(self):
        self.printSboxes()
        ar = []
        for s in self.sboxes:
            if not s.isDone() and (s.x_est.count("-") < 4 or s.y_est.count("-") < 4):
                for i, v in enumerate(s.x):
                    if s.x_est[i] == "-":
                        ar.append(v)
                for i, v in enumerate(s.y):
                    if s.y_est[i] == "-":
                        ar.append(v)
        ar.sort()
        ars = []
        for i in range(len(ar) - 1):
            if ar[i + 1] == ar[i]:
                ars.append(ar[i])
        r = self.presentRegstates(False)
        mnvar = None
        for i in ars:
            other = copy.deepcopy(self)
            other.fixVars([i], "d")
            rn = other.simpleEstimation()
            if rn[0] < r:
                mnvar = i
        return mnvar


    def simpleEstimation_hard(self):
            m = self.presentRegstates(False)
            self.fixVars([], [], False)
            n = self.presentRegstates(False)
            self.fixVars([], [], False)
            k = self.presentRegstates(False)
            ctr = 1
            for r in self.regstates[::-1]:
                if [self.values[i] for i in r].count("-") == k:
                    ctr += 1
                    nn = random.randint(0, len(r) - 1)
                    while self.values[r[nn]] != "-":
                        nn = (nn + 1) % len(r)
                    self.fixVars([r[nn]], ["d"], False)
                    kn = self.presentRegstates(False)
                    return m, n, k, kn

    def presentRegstates(self, pres=True):
        m = len(self.regstates[0])
        for r in self.regstates:
            tmp = [self.values[i] for i in r]
            if pres:
                print tmp, tmp.count("-")
                print r
            if tmp.count("-") < m:
                m = tmp.count("-")
        return m


    def Est(self):
        self.est = 1
        while "-" in self.values:
            if not "-" in self.values[:self.conf.master_key_length]:
                break
            self.startEstimate_max()

    def analyseEstInfo(self):
        d = {}
        for i in self.est_info:
            ind = "%d-%d" % (i[0], i[1])
            if ind in d:
                d[ind].append(i[2])
            else:
                d[ind] = [i[2]]
        mull_def = 1
        mull_und = 1
        for i in d:
            if i[-1] == "4":
                mull_def *= pow(pow(2, int(i[0])), len(d[i]))
            else:
                for j in d[i]:
                    if mean_amount(j, self.conf.sboxes[0]) > 1:
                        mull_und *= (mean_amount(j, self.conf.sboxes[0]) + 1)
        return mull_def * max(1, mull_und)

    def genRegVars(self):
        self.ctr = self.conf.master_key_length
        self.reg = range(self.conf.master_key_length)
        self.regstates.append(copy.deepcopy(self.reg))
        for i in xrange(self.conf.rounds - 1):
            self.reg = self.reg[self.conf.shift:] + self.reg[:self.conf.shift]
            for j in xrange(self.conf.sboxes_count):
                self.sboxes.append(Sbox(self.reg[-4 * (j + 1): None if j == 0 else -4 * j],
                                    [self.ctr, self.ctr + 1, self.ctr + 2, self.ctr + 3]))
                self.reg[-4 - 4 * j] = self.ctr
                self.reg[-3 - 4 * j] = self.ctr + 1
                self.reg[-2 - 4 * j] = self.ctr + 2
                self.reg[-1 - 4 * j] = self.ctr + 3
                self.ctr += 4
            self.regstates.append(copy.deepcopy(self.reg))

    def fixVarsLight(self, vars, vals):
        t = True
        while t or len(vals) > 0:
            t = False
            for i, v in enumerate(vars):
                w = str(vals[i])
                self.values[v] = str(w)
                self.updateSboxes(v, w)
            vars = []
            vals = []
            for i in self.sboxes:
                if i.isDefined() and not i.isDone() and not i.isPredicted():
                    t = True
                    d = i.getEntropyDelta()
                    self.est_info.append(
                            [d[0], d[1], ["".join(i.x_est), "".join(i.y_est)]])
                    d = i.estimateSbox()
                    for k in d:
                        vars.append(k)
                        vals.append(d[k])


    def fixVars(self, vars, vals, mx=False):
        t = True
        while t or len(vals) > 0:
            t = False
            for i, v in enumerate(vars):
                w = str(vals[i])
                self.values[v] = str(w)
                self.updateSboxes(v, w)
            vars = []
            vals = []

            for i in self.sboxes:
                if i.isDefined(mx) and not i.isDone():
                    t = True
                    d = i.getEntropyDelta()
                    self.est_info.append(
                            [d[0], d[1], ["".join(i.x_est), "".join(i.y_est)]])
                    d = i.estimateSbox()
                    for k in d:
                        vars.append(k)
                        vals.append(d[k])

    def updateSboxes(self, t, v):
        for s in self.sboxes:
            s.findVal(t, v)

    def countDifferentSboxes(self):
        d = {}
        for s in self.sboxes:
            x, y = s.countZeroes(s.x_est), s.countZeroes(s.y_est)
            ind = "%s-%s" % (min([x, y]), max([x, y]))
            if ind in d:
                d[ind] += 1
            else:
                d[ind] = 1

    def startEstimate_max(self):

        for i, s in enumerate(self.sboxes):
            if not s.isDone() and s.isDefined():
                d = s.getEntropyDelta()
                self.est_info.append(
                        [d[0], d[1], ["".join(s.x_est), "".join(s.y_est)]])
                dic = s.estimateSbox()
                vals = []
                vars = []
                for i in dic:
                    vals.append(i)
                    vars.append(dic[i])
                self.fixVars(vals, vars)
                return

        arr = []
        arr_good = []

        for i, s in enumerate(self.sboxes):
            s.calcAllEstimates()
            t = True
            if not s.isDone() and t:
                d = s.getEntropyDelta()
                if s.isGood():
                    arr_good.append([d[0], d[1], i])
                else:
                    arr.append([d[0], d[1], i])

        arr.sort()
        arr_good.sort()

        array = arr_good if len(arr_good) > 0 else arr

        d, d1, ind = array[0]
        self.est_info.append([d, d1, ["".join(self.sboxes[ind].x_est), "".join(self.sboxes[ind].y_est)]])

        dic = self.sboxes[ind].estimateSbox()

        vals = []
        vars = []
        for i in dic:
            vals.append(i)
            vars.append(dic[i])
        self.fixVars(vals, vars)
        r = []
        for i, v in enumerate(self.values):
            if v != 0:
                r.append(i)









    """
    ['-', '-', '-', 'd', 'd', 'd', 'd', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'd', '-', '-', '-', '-', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'd', '-', '-', '-', '-', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'd', '-', '-', '-', '-', '-', '-', '-', '-', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'd', '-', '-', '-', '-', '-', '-', '-', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'd', 'd', '-', '-', '-', '-', '-', '-', '-', '-', 'd', '1', '1', '1', 'd', '1', '1', '1', 'd', '-', '-', '-', '-', '1', '1', '1', '1', '1', '-', '-', '-', '-', '-', '-', '1', '-', '-', '-', '-', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '-', '-', '-', '-', '1', '-', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '-', '-', '-', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '-', '-', '-', '-']

    def calcMul(self):
        m = 1
        for i in self.values:
            m *= i
        return m

    """

'''
from random import randint
#vals0 = np.array([2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 19, 20, 21, 22, 23, 24,
#                  25, 26, 27, 28, 29, 30, 31, 35, 36, 37, 38, 39, 40, 41, 42, 44, 43, 45,
#                  46, 47, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63]) + 64 * 31
#vals1 = np.array([6, 22, 38, 54]) + 64 * 30
#valsres = list(vals0) + list(vals1)

vals1 = np.array([4, 6, 8, 10, 12, 14, 20, 22, 24, 26, 28, 30, 36, 38, 40, 42, 44, 46, 52, 54, 56, 58, 60, 62]) # 24
vals1 += 64 * 17
vals0 = np.array([0, 2, 8, 10, 16, 18, 24, 26, 32, 34, 40, 42, 48, 50, 56, 58]) # 16
vals0 += 64 * 16
vals2 = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 63, 62, 61, 60, 59, 58, 57, 56, 55, 54, 53, 52,
            46, 50, 34, 118, 12, 35, 19, 20, 21, 23, 26, 31, 38, 42, 110, 113
         #      34, 113, 19, 45, 20, 117, 21, 22, 26, 30, 12, 38, 13, 41, 14, 109, 15, 121, 16, 122, 17, 123, 18, 124,
         #      23, 24, 25, 27, 35, 36, 28, 29, 31, 32]
         ]


valsres = list(vals0) + list(vals1) + vals2

e = Estimator(valsres, ["d"] * len(valsres), Config("Present_80"))
e.Est()
#print e.analyseEstInfo()
#print valsres
#print

'''


# 8388608.0
#  786432.0
#  131072
#   32768
#    8192
#    2048
#     512
#     128


#12, 13,   19, 20,   21, 22,   23, 26,   24, 34
# 262144
# 32768
# 4096
# 1024
# 256
# 64
# 32
# 8

'''
dmin = 111111111111111111111
e = Estimator(valsres, ["d"] * len(valsres), Config("Present_80"))
for s in e.sboxes:
    #if not s.isDone():
        s.printSbox()
e.Est()
print e.analyseEstInfo()
v = []
#print valsres
'''

# { '1-4': [['----', '-ddd'], ['----', 'dd-d'], ['----', 'ddd-'], ['----', '-ddd'], ['----', 'ddd-'], ['-ddd', '----'], ['-ddd', '----'], ['----', 'd-dd'], ['-ddd', '----']], '2-4': [['----', 'd-d-'], ['--dd', '----'], ['----', 'd--d']], '3-4': [['----', '---d'], ['d---', '----'], ['d---', '----']]}
#   '1-4': [['----', '-ddd'], ['----', 'dd-d'], ['----', 'ddd-'], ['----', '-ddd'], ['----', 'ddd-'], ['-ddd', '----'], ['-ddd', '----'], ['----', 'd-dd'], ['-ddd', '----']], '2-4': [['----', 'd-d-'], ['--dd', '----'], ['----', 'd--d']], '3-4': [['----', '---d'], ['d---', '----'], ['d---', '----']]}



'''
for j in range(128):
    print j
    if j not in valsres and j not in v:
        for k in range(j + 1, 128):
            if k not in valsres and k not in v:
                v = [j, k]
   # print v
                e = Estimator(valsres + v, ["1"] * len(valsres + v), Config("Present_80"))
                e.Est()
                d = e.analyseEstInfo()
                if d < dmin:
                    dmin = d
                    print d, v

# 524288 [36, 42, 78], [34, 45, 46], [47, 12, 35], [29, 36, 42]
# 262144 [46, 79, 42]



rounds = 31
vals1 = np.array([4, 6, 8, 10, 12, 14, 20, 22, 24, 26, 28, 30,
                 36, 38, 40, 42, 44, 46, 52, 54, 56, 58, 60, 62])
vals1 += 64 * (rounds - 1)
vals0 = np.array([0, 2, 8, 10, 16, 18, 24, 26, 32, 34, 40, 42, 48, 50, 56, 58])
vals0 += 64 * (rounds - 2)
vals2 = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 63, 62, 61, 60, 59, 58, 57, 56, 55, 54, 53, 52]
valsres = list(vals0) + list(vals1) + vals2
#vals0 = [21, 22, 25, 26, ]
#valsres = [1124, 1933, 531, 134, 1137, 1831, 1273, 888, 246, 722, 1258, 1095, 711, 1764, 1416, 404, 978, 1827, 55, 1795, 939, 380, 1796, 191, 719, 1352, 326, 1623, 832, 787, 614, 1804, 1806, 523, 912, 413, 683, 1214, 170, 828, 390, 1701, 2001, 1447, 381, 1823, 1535, 207, 394, 255, 1084, 1875, 1870, 31, 70, 865, 264, 1357, 834, 1947, 1193, 499, 1522, 1716, 182, 1617, 239, 742, 274, 914, 1244, 75, 1671, 1205, 1062, 113, 1287, 903, 698, 1158, 21, 541, 1733, 550, 1228, 769, 1338, 1476, 419, 1113, 1752, 174, 667, 1919, 241, 805, 905, 1433, 1387, 1748, 1075, 701, 9, 1220, 1039, 1374, 1046, 172, 927, 817, 1164, 1681, 1454, 1528, 1175, 294, 482, 1951, 536, 1763, 1348, 1746, 1037, 276, 1917, 565, 1637, 799, 438, 36, 1988, 317, 297, 1480, 1365, 1593, 1174, 1698, 1078, 1345]
#valsres = [663, 641, 1874, 1506, 1430, 1945, 704, 848, 1055, 734, 75, 535, 767, 563, 485, 94, 830, 233, 1833, 1746, 738, 426, 1936, 749, 1530, 973, 677, 1053, 432, 825, 1791, 403, 1283, 841, 2004, 947, 699, 1444, 202, 2043, 776, 694, 1797, 1045, 278, 107, 401, 280, 1905, 1947, 1649, 1384, 303, 670, 458, 1913, 325, 1651, 1508, 1383, 1338, 1020, 1335, 1220, 190, 369, 1736, 1765, 1722, 300, 206, 1212, 1172, 1720, 2032, 1658, 885, 992, 231, 135, 537, 352, 868, 1645, 10, 702, 1729, 1629, 218, 295, 24, 1328, 185, 1084, 1886, 1193, 108, 456, 110, 932, 695, 899, 1580, 1836, 130, 52, 400, 1302, 816, 1263, 1498, 1810, 1473, 925, 1011, 820, 1713, 967, 1217, 55, 997, 170, 1349, 1994, 86, 1532, 19, 961, 809, 1086]
#valsres = [1991, 1105, 1135, 248, 739, 11, 1073, 846, 1918, 543, 784, 1078, 1316, 84, 762, 1082, 1207, 2047, 1839, 100, 550, 810, 680, 2043, 7, 1654, 783, 462, 1624, 416, 1043, 1639, 1101, 2010, 402, 901, 59, 1062, 1286, 413, 125, 1550, 1034, 778, 1658, 1003, 708, 1532, 1127, 228, 534, 1892, 1039, 946, 503, 1171, 1604, 1191, 1066, 1912, 995, 911, 1635, 542, 694, 1832, 1234, 546, 400, 470, 1619, 399, 1268, 1357, 419, 1454, 1755, 2042, 55, 973, 906, 1072, 241, 864, 1819, 57, 1314, 1762, 772, 626, 69, 268, 696, 1601, 1937, 1857, 581, 842, 689, 513, 120, 1715, 1402, 869, 1793, 1830, 1831, 1540, 1827, 1473, 1313, 815, 87, 505, 1919, 372, 1251, 691, 199, 1137]
#valsres = [1631, 1838, 363, 797, 1967, 1253, 1553, 128, 1878, 549, 430, 578, 1450, 1673, 444, 1047, 676, 1296, 1545, 23, 1833, 1902, 1646, 713, 1937, 695, 360, 1552, 1329, 861, 95, 1621, 1871, 1003, 850, 567, 892, 478, 1764, 672, 1266, 628, 1301, 1551, 410, 1430, 752, 1419, 1745, 821, 384, 398, 1668, 1240, 373, 941, 1635, 1548, 645, 1053, 1368, 77, 1256, 1438, 270, 1643, 1962, 313, 372, 50, 955, 2037, 1638, 88, 1385, 486, 504, 66, 830, 770, 1138, 1583, 1265, 346, 542, 1257, 1845, 996, 435, 222, 16, 1832, 1068, 1692, 687, 2038, 1086, 652, 1649, 511, 2011, 846, 1027, 1000, 1528, 1485, 340, 321, 1727, 1803]
valsres = [1826, 10, 570, 1359, 1611, 1230, 648, 74, 1773, 379, 1370, 505, 147, 663, 191, 1930, 960, 324, 1789, 1042, 515, 1700, 18, 1596, 821, 932, 114, 825, 429, 1161, 685, 1858, 465, 907, 1257, 1202, 1066, 1643, 1381, 1637, 428, 762, 186, 1949, 897, 1144, 1512, 1994, 214, 1899, 1667, 858, 1333, 362, 981, 799, 1366, 1145, 748, 524, 1156, 1801, 1050, 1556, 1467, 980, 1394, 1875, 1368, 711, 1080, 1133, 486, 892, 1763, 296, 666, 1494, 1466, 1585, 640, 1228, 1139, 347, 1882, 508, 1703, 139, 1470, 1398, 1719, 1739, 793, 629, 227, 1300, 133, 107, 202, 700]

vals0 = np.array([2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 19, 20, 21, 22, 23, 24,
                  25, 26, 27, 28, 29, 30, 31, 35, 36, 37, 38, 39, 40, 41, 42, 44, 43, 45,
                  46, 47, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63]) + 64 * 31
vals1 = np.array([6, 22, 38, 54]) + 64 * 30
valsres = list(vals0) + list(vals1)

#valsres = [805, 1891, 1431, 433, 345, 1330, 536, 650, 823, 569, 619, 975, 875, 133, 1924, 161, 1378, 478, 1288, 1447, 1252, 1429, 1738, 108, 1177, 1006, 1973, 1884, 20, 890, 958, 1282, 1057, 506, 104, 491, 128, 1452, 1210, 292, 1732, 1497, 1941, 1996, 1729, 1875, 1435, 942, 985, 1812, 407, 317, 968, 1947, 545, 1571, 1303, 613, 1021, 1293, 1108, 732, 1188, 132, 1629, 1853, 1448, 663, 527, 1137, 2030, 125, 760, 672, 810, 54, 178, 361, 1224, 1207, 1911, 142, 635, 452, 1803, 644, 762, 395, 256, 677, 1309, 1815, 1591, 1200, 1264, 1088, 1523, 171, 556, 798, 604, 1100, 78, 1574, 1036, 615, 68, 861, 122, 1772, 667, 1846, 806, 599, 1132, 922, 1953, 885, 1034, 515, 868, 320, 1736, 439, 404, 1278, 510, 602, 1454, 1456, 1169, 1560, 1531, 331, 1074, 2047, 1261, 1161, 1297, 1120, 1104, 1539, 1168, 960, 1507, 1489, 1568, 803, 145, 1632]
#valsres = list(np.arange(64) + 64) + list(np.arange(3, 19))
#valsres = range(64) + list(np.arange(3, 18) + 64)
#valsres = [773, 1242, 897, 865, 142, 246, 891, 1141, 615, 264, 184, 58, 260, 517, 919, 1292, 90, 1220, 1680, 1286, 2007, 1852, 1685, 2011, 1120, 967, 875, 721, 612, 37, 621, 1792, 764, 1147, 1936, 1723, 1829, 1058, 1350, 670, 216, 400, 244, 1530, 901, 1020, 972, 911, 1057, 500, 249, 1047, 437, 1522, 1026, 854, 1767, 1638, 1927, 470, 102, 1679, 1408, 427, 572, 706, 1941, 904, 1051, 1380, 1340, 1000, 1631, 116, 1855, 926, 1935, 499, 547, 1188, 432, 336, 57, 1389, 1572, 508, 1318, 921, 2005, 228, 1075, 957, 1368, 937, 586, 1203, 1431, 405, 1208, 133, 1209, 1227, 762, 331, 858, 1402, 1627, 1123, 78, 999, 1824, 529, 1964, 684, 414, 887, 778, 59, 1734, 796, 916, 1900, 1444, 1768, 705, 780, 669, 1024, 1168, 1791, 558, 1182, 316]

e = Estimator(valsres)#range(64) + list(np.arange(3, 18) + 64)) #range(64) + list(np.arange(3, 18) + 64))
#print pow(2, 79)
print e.m#**(0.5)# ** 0.5 #/ 2
print e.values
#print pow (2, 40)
'''