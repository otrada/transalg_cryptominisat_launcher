import copy
import numpy as np

from fractions import Fraction


def getComb(n):
    arr = [1, 1]
    for i in xrange(n - 1):
        arrt = [1]
        for j in xrange(len(arr) - 1):
            arrt.append(arr[j] + arr[j + 1])
        arrt.append(1)
        arr = arrt
    return arr


print getComb(4)


def getProb(o1, o2):
    omax = max(o1, o2)
    omin = min(o1, o2)
    den = pow(2, omin)
    p = pow(2, 4 - omax)
    # print den, p
    s = Fraction((1.0 - 1.0 / den) ** p)
    # print s
    arr = getComb(p)
    # print arr
    for i in xrange(1, p + 1):
        s += arr[i] * Fraction(i * ((1.0 - 1.0 / den) ** (p - i)) * ((1.0 / den) ** i))
        # print arr[i], i, (1.0 - 1.0 / den) ** i, Fraction((1.0 / den) ** (p - i)), \
        #    Fraction(((1.0 - 1.0 / den) ** (p -i))), Fraction(((1.0 / den) ** (p - i)))
    return (s)



class Sbox():
    def __init__(self, start_index, end_index):
        self.x = start_index
        self.y = end_index
        self.x_est = [0] * 4
        self.y_est = [0] * 4
        self.x_all_est = 0
        self.y_all_est = 0

    def calcEstimate(self, v):
        c = 1
        for i in v:
            c *= 2 if i == 0 else i
        return c

    def calcEstimate_(self, v):
        c = 1
        for i in v:
            c *= 1 if i == 0 else i
        return c

    def calcAllEstimates(self):
        self.x_all_est = self.calcEstimate(self.x_est)
        self.y_all_est = self.calcEstimate(self.y_est)
        return self.x_all_est, self.y_all_est

    def countZeroes(self, v):
        c = 0
        for i in v:
            if i == 0:
                c += 1
        return c

    def getRoot(self, set, v):
        # print set
        c = self.countZeroes(set)
        try:
            return v ** (1.0 / c)
        except:
            return 1

    def notOnes(self, set, v):
        # print set
        c = 0
        for i in set:
            if i != 1:
                c += 1
        return v ** (1.0 / c)

    def estimateSbox(self, d):
        print self.x_est, self.y_est
        self.calcAllEstimates()
        minest = min(self.x_all_est, self.y_all_est)
        # if self.countZeroes(self.x_est) + self.countZeroes(self.y_est) <= 2:
        #    minest = min(self.calcEstimate_(self.x_est), self.calcEstimate_(self.y_est))

        # v = pow(2, min(self.countZeroes(self.x_est), self.countZeroes(self.y_est)))
        dic = {}

        # if self.x_all_est <= self.y_all_est:
        print minest, self.x_est
        if self.x_all_est == minest and self.countZeroes(self.x_est) < self.countZeroes(self.y_est):
            set1 = self.x_est
            set1_ = self.x
            set2 = self.y_est
            set2_ = self.y
        else:
            set1 = self.y_est
            set1_ = self.y
            set2 = self.x_est
            set2_ = self.x
        print set1, set1_, set2, set2_
        v = getProb(4 - self.countZeroes(self.x_est), 4 - self.countZeroes(self.y_est))
        print "v", v
        # v = minest / self.calcEstimate_(set1)
        r = self.getRoot(set1, v)
        print "r", r
        for i in xrange(4):
            if set1[i] == 0:
                set1[i] = r
                dic[set1_[i]] = r
        r = self.notOnes(set2, v)
        for i in xrange(4):
            if set2[i] != 1:  # TODO
                set2[i] = 1  # r
                dic[set2_[i]] = 1  # r

        self.calcAllEstimates()
        return dic

    def goodEstimateSbox(self):
        self.calcAllEstimates()
        dic = {}
        for i in xrange(4):
            if self.x_est[i] == 0:
                self.x_est[i] = 1
                dic[self.x[i]] = 1
        for i in xrange(4):
            if self.y_est[i] == 0:
                self.y_est[i] = 1
                dic[self.y[i]] = 1
        self.calcAllEstimates()
        # if self.x_all_est >= self.y_all_est:
        #    for i in range(4):
        #        if self.x_est[i] != 1:
        #            self.x_est[i] = 1
        #            dic[self.x[i]] = 1
        # else:
        #    for i in range(4):
        #        if self.y_est[i] != 1:
        #            self.y_est[i] = 1
        #            dic[self.y[i]] = 1
        # self.calcAllEstimates()
        # v = self.getRoot(self.x_est,
        #                 1.0 * self.y_all_est / self.calcEstimate_(self.x_est))
        # for i in range(4):
        #    self.x_est[i] = 1
        #    dic[self.x[i]] = 1
        #    if self.x_est[i] == 0:
        #        self.x_est[i] = v
        #        dic[self.x[i]] = v

        # if self.y_est[i] == 0:
        #    self.y_est[i] = v
        #    dic[self.y[i]] = v
        self.calcAllEstimates()
        return dic

    def isGood(self):
        self.calcAllEstimates()
        # print self.x_all_est,
        if self.x_all_est * self.y_all_est <= 16:
            return True
        return False

    def findVal(self, v, vl):
        dic = {}
        if v in self.x:
            self.x_est[self.x.index(v)] = vl
        if v in self.y:
            self.y_est[self.y.index(v)] = vl
        return dic

    def isDefined(self):
        if self.countZeroes(self.x_est) == 0 or self.countZeroes(self.y_est) == 0:
            return True
        return False

    def isDone(self):
        # print self.countZeroes(self.x_est), self.countZeroes(self.y_est)
        if self.countZeroes(self.x_est) == 0 and self.countZeroes(self.y_est) == 0:
            return True
        return False

    def getEntropyDelta(self):
        self.calcAllEstimates()

        return min(self.countZeroes(self.x_est), self.countZeroes(self.y_est)), \
               max(self.countZeroes(self.x_est), self.countZeroes(self.y_est))

        # return max(self.countZeroes(self.x_est), self.countZeroes(self.y_est)) - min(self.countZeroes(self.x_est), self.countZeroes(self.y_est))


# s = Sbox([5, 6, 7, 8], [1, 2, 3, 4])
# s.x_est = [1, 1, 1, 0]
# s.y_est = [1, 1, 0, 0]


class Estimator():
    def __init__(self, variables):
        self.ctr = 80
        self.sboxes = []
        self.reg = xrange(80)
        self.regstates = []
        self.values = [0] * (80 + 4 * 31)
        self.genRegVars()
        print len(variables)
        vrs = copy.deepcopy(variables)
        for i in xrange(len(vrs)):
            v = variables[i]
            vd, vm = v / 64, v % 64
            # print v,
            # if ty == 0:
            # vd, vm = v / 64, v % 64
            v = (self.regstates[vd])[vm]
            vrs[i] = v
        self.fixVars(vrs, [1] * len(vrs))
        self.m = 1

        for i in xrange(len(self.regstates)):
            c = [self.values[j] for j in self.regstates[i]].count(0)
            print c, i, [self.values[j] for j in self.regstates[i]]

        for i in self.sboxes:
            i.calcAllEstimates()
            print i.x_est, i.y_est, i.x, i.y

        while 0 in self.values:
            self.startEstimate()

        self.m = 1
        for i in self.values:
            self.m *= i
        # self.m = self.m ** 0.5
        for i in self.sboxes:
            i.calcAllEstimates()
            print i.x_est, i.y_est, i.x_all_est, i.y_all_est

    def genRegVars(self):
        self.regstates.append(copy.deepcopy(self.reg))
        for i in xrange(31):
            self.reg = self.reg[19:] + self.reg[:19]
            self.sboxes.append(Sbox(self.reg[-4:],
                                    [self.ctr, self.ctr + 1, self.ctr + 2, self.ctr + 3]))
            self.reg[-4] = self.ctr
            self.reg[-3] = self.ctr + 1
            self.reg[-2] = self.ctr + 2
            self.reg[-1] = self.ctr + 3
            self.ctr += 4
            self.regstates.append(copy.deepcopy(self.reg))

    def fixVars(self, vars, vals):
        while len(vars) > 0:
            v = vars[0]
            # print v,
            # if ty == 0:
            #    vd, vm = v / 64, v % 64
            #    v = (self.regstates[vd][::-1])[vm]
            # print v, vals[0]
            self.values[v] = vals[0]
            print v, vals[0]
            self.updateSboxes(v, vals[0])
            vars = vars[1:]
            vals = vals[1:]

            for i in self.sboxes:
                if i.isDefined() and not i.isDone():
                    d = i.estimateSbox(1)
                    for k in d:
                        vars.append(k)
                        vals.append(d[k])

    def updateSboxes(self, t, v):
        for s in self.sboxes:
            s.findVal(t, v)

    def startEstimate(self):
        # for i in self.sboxes:
        #    if i.isDone == False:
        #        print i.x, i.x_est, i.y, i.y_est
        arr = []
        for i, s in enumerate(self.sboxes):
            if not s.isDone() and s.isDefined():
                dic = s.estimateSbox()
                vals = []
                vars = []
                for i in dic:
                    vals.append(i)
                    vars.append(dic[i])
                self.fixVars(vals, vars)
                return
        '''
        for i, s in enumerate(self.sboxes):
            #print "GGGGGGGGGGGG", s.isDone(), s.x_est, s.y_est,  s.isGood()
            if not s.isDone() and s.isGood():
                #print s.x, s.y
                dic = s.goodEstimateSbox()
                vals = []
                vars = []
                for i in dic:
                    vals.append(i)
                    vars.append(dic[i])
                self.fixVars(vals, vars)
                #print self.values
                return
        '''

        for i, s in enumerate(self.sboxes):
            s.calcAllEstimates()
            t = False
            for j in (s.x + s.y):
                if 0 <= j <= 79:
                    t = True
                    break
            # print s.x, s.y, t
            if not s.isDefined() and t:
                # print "VVVVVVVVVVVVVVV"
                d = s.getEntropyDelta()
                arr.append([d[0], d[1], i])
                # if not s.isDefined():#s.countZeroes(s.x_est) != 0 or s.countZeroes(s.y_est) != 0:
                #    arr.append([min(s.x_all_est, s.y_all_est), max(s.x_all_est, s.y_all_est), i])

        arr.sort()
        print arr
        # print self.values
        # if len(arr) == 0:
        # print self.values
        d, d1, ind = arr[0]
        # print self.sboxes[ind].x_est, self.sboxes[ind].y_est
        # if (self.sboxes[ind].x_all_est <= self.sboxes[ind].y_all_est):
        #    self.m *= pow(2, self.sboxes[ind].countZeroes(self.sboxes[ind].x_est))
        # else:
        #    self.m *= pow(2, self.sboxes[ind].countZeroes(self.sboxes[ind].y_est))
        # print self.m
        dic = self.sboxes[ind].estimateSbox(d)
        # print self.sboxes[ind].x_est, self.sboxes[ind].y_est
        vals = []
        vars = []
        for i in dic:
            vals.append(i)
            vars.append(dic[i])
        self.fixVars(vals, vars)
        print self.values
        r = []
        for i, v in enumerate(self.values):
            if v != 0:
                r.append(i)
        print r

    def calcMul(self):
        m = 1
        for i in self.values:
            m *= i
        return m



rounds = 31
vals1 = np.array([4, 6, 8, 10, 12, 14, 20, 22, 24, 26, 28, 30,
                 36, 38, 40, 42, 44, 46, 52, 54, 56, 58, 60, 62])
vals1 += 64 * (rounds - 1)
vals0 = np.array([0, 2, 8, 10, 16, 18, 24, 26, 32, 34, 40, 42, 48, 50, 56, 58])
vals0 += 64 * (rounds - 2)
vals2 = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 63, 62, 61, 60, 59, 58, 57, 56, 55, 54, 53, 52]
valsres = list(vals0) + list(vals1) + vals2
#vals0 = [21, 22, 25, 26, ]
#valsres = [1124, 1933, 531, 134, 1137, 1831, 1273, 888, 246, 722, 1258, 1095, 711, 1764, 1416, 404, 978, 1827, 55, 1795, 939, 380, 1796, 191, 719, 1352, 326, 1623, 832, 787, 614, 1804, 1806, 523, 912, 413, 683, 1214, 170, 828, 390, 1701, 2001, 1447, 381, 1823, 1535, 207, 394, 255, 1084, 1875, 1870, 31, 70, 865, 264, 1357, 834, 1947, 1193, 499, 1522, 1716, 182, 1617, 239, 742, 274, 914, 1244, 75, 1671, 1205, 1062, 113, 1287, 903, 698, 1158, 21, 541, 1733, 550, 1228, 769, 1338, 1476, 419, 1113, 1752, 174, 667, 1919, 241, 805, 905, 1433, 1387, 1748, 1075, 701, 9, 1220, 1039, 1374, 1046, 172, 927, 817, 1164, 1681, 1454, 1528, 1175, 294, 482, 1951, 536, 1763, 1348, 1746, 1037, 276, 1917, 565, 1637, 799, 438, 36, 1988, 317, 297, 1480, 1365, 1593, 1174, 1698, 1078, 1345]
#valsres = [663, 641, 1874, 1506, 1430, 1945, 704, 848, 1055, 734, 75, 535, 767, 563, 485, 94, 830, 233, 1833, 1746, 738, 426, 1936, 749, 1530, 973, 677, 1053, 432, 825, 1791, 403, 1283, 841, 2004, 947, 699, 1444, 202, 2043, 776, 694, 1797, 1045, 278, 107, 401, 280, 1905, 1947, 1649, 1384, 303, 670, 458, 1913, 325, 1651, 1508, 1383, 1338, 1020, 1335, 1220, 190, 369, 1736, 1765, 1722, 300, 206, 1212, 1172, 1720, 2032, 1658, 885, 992, 231, 135, 537, 352, 868, 1645, 10, 702, 1729, 1629, 218, 295, 24, 1328, 185, 1084, 1886, 1193, 108, 456, 110, 932, 695, 899, 1580, 1836, 130, 52, 400, 1302, 816, 1263, 1498, 1810, 1473, 925, 1011, 820, 1713, 967, 1217, 55, 997, 170, 1349, 1994, 86, 1532, 19, 961, 809, 1086]
#valsres = [1991, 1105, 1135, 248, 739, 11, 1073, 846, 1918, 543, 784, 1078, 1316, 84, 762, 1082, 1207, 2047, 1839, 100, 550, 810, 680, 2043, 7, 1654, 783, 462, 1624, 416, 1043, 1639, 1101, 2010, 402, 901, 59, 1062, 1286, 413, 125, 1550, 1034, 778, 1658, 1003, 708, 1532, 1127, 228, 534, 1892, 1039, 946, 503, 1171, 1604, 1191, 1066, 1912, 995, 911, 1635, 542, 694, 1832, 1234, 546, 400, 470, 1619, 399, 1268, 1357, 419, 1454, 1755, 2042, 55, 973, 906, 1072, 241, 864, 1819, 57, 1314, 1762, 772, 626, 69, 268, 696, 1601, 1937, 1857, 581, 842, 689, 513, 120, 1715, 1402, 869, 1793, 1830, 1831, 1540, 1827, 1473, 1313, 815, 87, 505, 1919, 372, 1251, 691, 199, 1137]
#valsres = [1631, 1838, 363, 797, 1967, 1253, 1553, 128, 1878, 549, 430, 578, 1450, 1673, 444, 1047, 676, 1296, 1545, 23, 1833, 1902, 1646, 713, 1937, 695, 360, 1552, 1329, 861, 95, 1621, 1871, 1003, 850, 567, 892, 478, 1764, 672, 1266, 628, 1301, 1551, 410, 1430, 752, 1419, 1745, 821, 384, 398, 1668, 1240, 373, 941, 1635, 1548, 645, 1053, 1368, 77, 1256, 1438, 270, 1643, 1962, 313, 372, 50, 955, 2037, 1638, 88, 1385, 486, 504, 66, 830, 770, 1138, 1583, 1265, 346, 542, 1257, 1845, 996, 435, 222, 16, 1832, 1068, 1692, 687, 2038, 1086, 652, 1649, 511, 2011, 846, 1027, 1000, 1528, 1485, 340, 321, 1727, 1803]
valsres = [1826, 10, 570, 1359, 1611, 1230, 648, 74, 1773, 379, 1370, 505, 147, 663, 191, 1930, 960, 324, 1789, 1042, 515, 1700, 18, 1596, 821, 932, 114, 825, 429, 1161, 685, 1858, 465, 907, 1257, 1202, 1066, 1643, 1381, 1637, 428, 762, 186, 1949, 897, 1144, 1512, 1994, 214, 1899, 1667, 858, 1333, 362, 981, 799, 1366, 1145, 748, 524, 1156, 1801, 1050, 1556, 1467, 980, 1394, 1875, 1368, 711, 1080, 1133, 486, 892, 1763, 296, 666, 1494, 1466, 1585, 640, 1228, 1139, 347, 1882, 508, 1703, 139, 1470, 1398, 1719, 1739, 793, 629, 227, 1300, 133, 107, 202, 700]

vals0 = np.array([2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 19, 20, 21, 22, 23, 24,
                  25, 26, 27, 28, 29, 30, 31, 35, 36, 37, 38, 39, 40, 41, 42, 44, 43, 45,
                  46, 47, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63]) + 64 * 31
vals1 = np.array([6, 22, 38, 54]) + 64 * 30
valsres = list(vals0) + list(vals1)

#valsres = [805, 1891, 1431, 433, 345, 1330, 536, 650, 823, 569, 619, 975, 875, 133, 1924, 161, 1378, 478, 1288, 1447, 1252, 1429, 1738, 108, 1177, 1006, 1973, 1884, 20, 890, 958, 1282, 1057, 506, 104, 491, 128, 1452, 1210, 292, 1732, 1497, 1941, 1996, 1729, 1875, 1435, 942, 985, 1812, 407, 317, 968, 1947, 545, 1571, 1303, 613, 1021, 1293, 1108, 732, 1188, 132, 1629, 1853, 1448, 663, 527, 1137, 2030, 125, 760, 672, 810, 54, 178, 361, 1224, 1207, 1911, 142, 635, 452, 1803, 644, 762, 395, 256, 677, 1309, 1815, 1591, 1200, 1264, 1088, 1523, 171, 556, 798, 604, 1100, 78, 1574, 1036, 615, 68, 861, 122, 1772, 667, 1846, 806, 599, 1132, 922, 1953, 885, 1034, 515, 868, 320, 1736, 439, 404, 1278, 510, 602, 1454, 1456, 1169, 1560, 1531, 331, 1074, 2047, 1261, 1161, 1297, 1120, 1104, 1539, 1168, 960, 1507, 1489, 1568, 803, 145, 1632]
#valsres = list(np.arange(64) + 64) + list(np.arange(3, 19))
#valsres = range(64) + list(np.arange(3, 18) + 64)
#valsres = [773, 1242, 897, 865, 142, 246, 891, 1141, 615, 264, 184, 58, 260, 517, 919, 1292, 90, 1220, 1680, 1286, 2007, 1852, 1685, 2011, 1120, 967, 875, 721, 612, 37, 621, 1792, 764, 1147, 1936, 1723, 1829, 1058, 1350, 670, 216, 400, 244, 1530, 901, 1020, 972, 911, 1057, 500, 249, 1047, 437, 1522, 1026, 854, 1767, 1638, 1927, 470, 102, 1679, 1408, 427, 572, 706, 1941, 904, 1051, 1380, 1340, 1000, 1631, 116, 1855, 926, 1935, 499, 547, 1188, 432, 336, 57, 1389, 1572, 508, 1318, 921, 2005, 228, 1075, 957, 1368, 937, 586, 1203, 1431, 405, 1208, 133, 1209, 1227, 762, 331, 858, 1402, 1627, 1123, 78, 999, 1824, 529, 1964, 684, 414, 887, 778, 59, 1734, 796, 916, 1900, 1444, 1768, 705, 780, 669, 1024, 1168, 1791, 558, 1182, 316]

e = Estimator(valsres)#range(64) + list(np.arange(3, 18) + 64)) #range(64) + list(np.arange(3, 18) + 64))
#print pow(2, 79)
print e.m#**(0.5)# ** 0.5 #/ 2
print e.values
#print pow (2, 40)
