class LBlock:
    def __init__(self, key, rounds=32):
        """Create a PRESENT cipher object
        key:    the key as a 128-bit or 80-bit rawstring
        rounds: the number of rounds as an integer, 32 by default
        """
        self.rounds = rounds
        #if len(key) * 8 == 80:
        self.roundkeys = generateRoundkeys80(string2number(key), self.rounds)
        #elif len(key) * 8 == 128:
        #    self.roundkeys = generateRoundkeys128(string2number(key), self.rounds)
        #else:
        #    raise ValueError, "Key must be a 128-bit or 80-bit rawstring"

    def encrypt(self, blocks):
        """Encrypt 1 block (8 bytes)
        Input:  plaintext block as raw string
        Output: ciphertext block as raw string
        """
        c = []
        for block in blocks:
            state = string2number(block)
            for i in xrange(self.rounds - 1):
                state = addRoundKey(state, self.roundkeys[i])
                state = sBoxLayer(state)
                state = pLayer(state)
            #cipher = state
            #if self.rounds == 32:
            cipher = addRoundKey(state, self.roundkeys[-1])
            c.append(number2string_N(cipher, 8))
        return c

    def decrypt(self, block):
        """Decrypt 1 block (8 bytes)
        Input:  ciphertext block as raw string
        Output: plaintext block as raw string
        """
        state = string2number(block)
        for i in xrange(self.rounds - 1):
            state = addRoundKey(state, self.roundkeys[-i - 1])
            state = pLayer_dec(state)
            state = sBoxLayer_dec(state)
        decipher = addRoundKey(state, self.roundkeys[0])
        return number2string_N(decipher, 8)

    def get_block_size(self):
        return 8

# 0   1   2   3   4   5   6   7   8   9   a   b   c   d   e   f
Sbox = [0xc, 0x5, 0x6, 0xb, 0x9, 0x0, 0xa, 0xd, 0x3, 0xe, 0xf, 0x8, 0x4, 0x7, 0x1, 0x2]
Sbox_inv = [Sbox.index(x) for x in xrange(16)]
PBox = [ 0, 16, 32, 48,    1, 17, 33, 49,    2, 18, 34, 50,    3, 19, 35, 51,
         4, 20, 36, 52,    5, 21, 37, 53,    6, 22, 38, 54,    7, 23, 39, 55,
         8, 24, 40, 56,    9, 25, 41, 57,   10, 26, 42, 58,   11, 27, 43, 59,
        12, 28, 44, 60,   13, 29, 45, 61,   14, 30, 46, 62,   15, 31, 47, 63]
PBox_inv = [PBox.index(x) for x in xrange(64)]

S8 = [8, 7, 14, 5, 15, 13, 0, 6, 11, 12, 9, 10, 2, 4, 1, 3]
S9 = [11, 5, 15, 0, 7, 2, 9, 13, 4, 8, 1, 12, 14, 10, 3, 6]


def generateRoundkeys80(key, rounds):
    roundkeys = []
    for i in xrange(1, rounds + 1):  # (K1 ... K32)
        roundkeys.append(key >> 48)
        key = ((key & (2 ** 51 - 1)) << 29) + (key >> 51)
        #key ^= (S8[key >> 76] << 76)
        key = (S8[key >> 76] << 76) + (S9[(key >> 72) & 0xF] << 72) + (key & (2 ** 72 - 1))
        key = key ^ (i << 46)
    return roundkeys


def generateRoundkeys128(key, rounds):

    roundkeys = []
    for i in xrange(1, rounds + 1):  # (K1 ... K32)
        # rawkey: used in comments to show what happens at bitlevel
        roundkeys.append(key >> 64)
        # 1. Shift
        key = ((key & (2 ** 51 - 1)) << 29) + (key >> 51)
        # 2. SBox
        key = (Sbox[key >> 124] << 124) + (Sbox[(key >> 120) & 0xF] << 120) + (key & (2 ** 120 - 1))
        # 3. Salt
        key ^= i << 2
    return roundkeys


def addRoundKey(state, roundkey):
    return state ^ roundkey


def sBoxLayer(state):
    """SBox function for encryption
    Input:  64-bit integer
    Output: 64-bit integer"""

    output = 0
    for i in xrange(16):
        output += Sbox[(state >> (i * 4)) & 0xF] << (i * 4)
    return output


def sBoxLayer_dec(state):
    """Inverse SBox function for decryption
    Input:  64-bit integer
    Output: 64-bit integer"""
    output = 0
    for i in xrange(16):
        output += Sbox_inv[( state >> (i * 4)) & 0xF] << (i * 4)
    return output


def pLayer(state):
    """Permutation layer for encryption
    Input:  64-bit integer
    Output: 64-bit integer"""
    output = 0
    for i in xrange(64):
        output += ((state >> i) & 0x01) << PBox[i]
    return output


def pLayer_dec(state):
    """Permutation layer for decryption
    Input:  64-bit integer
    Output: 64-bit integer"""
    output = 0
    for i in xrange(64):
        output += ((state >> i) & 0x01) << PBox_inv[i]
    return output


def string2number(i):
    """ Convert a string to a number
    Input: string (big-endian)
    Output: long or integer
    """
    return int(i.encode('hex'), 16)


def number2string_N(i, N):
    """Convert a number to a string of fixed size
    i: long or integer
    N: length of string
    Output: string (big-endian)
    """
    s = '%0*x' % (N * 2, i)
    return s.decode('hex')


def _test():
    import doctest

    doctest.testmod()


if __name__ == "__main__":
    #key = "0123456789abcdef0123456789abcdef".decode('hex')
    key = "0123456789abcdef0123".decode('hex')

    plain_2 = "23444444"

    cipher = Present(key)
    e = cipher.encrypt(plain_2)
    decrypted_2 = cipher.decrypt(e)
    print decrypted_2

