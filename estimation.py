# -*- coding: utf-8 -*-

import json
from Experiments_launcher import ExperimentLauncher
from Estimator import Estimator, Config
import matplotlib.pyplot as plt
import numpy as np
from scipy import stats
import math

'''
ind = 0
sol = []
#x = []
#y_keys = []
#y_all = []
#y_all_ = []
f = open("ex")
for line in f:
    if line[:3] == "EXP":
        pass
        #print line[:-1]#, sol[ind][-2]["time"]
    else:
        s = line.split(" ")
        #print s

        sol.append(s)

        #if float(s[2]) != sol[ind][-1]["time"]:
        #    ind += 1
        #if int(s[0]) < 15000:
        #    x.append(int(s[0]) - 1)
        #    y_keys.append(float(s[1]))
        #    y_all.append(float(s[2]))
        #    y_all_.append(sol[ind][-2]["time"])
        #    ind += 1


x1 = []
x2 = []
x3 = []
x = []

f = open("exxxp_")
ind = 0
vals = solutions = key = None
for line in f:
    try:
        #print line
        if line[:3] == "EXP":
            #print line
            pass

        elif line[:2] == "[{":

            solutions = json.loads(line)
            if float(sol[ind][2]) != solutions[-1]["time"]:
                ind += 1
                if ind > len(sol):
                    break
            vls = []
            vrs = []
            for i, v in enumerate(vals):
                if v != '-':
                    vls.append(i)
                    vrs.append(v)
            #l = fn.readline()
            #ll = l.split(" ")[0]
            #le = l.split(" ")[-1]
            # if ll in d:
            #    d[ll].append()
            est = Estimator(vls, vrs, Config("Present_80"))
            #if ll != 'EXP:' and int(ll) > 10000:
            #    print ll
            #    est.presentRegstates()
            #    for sb in est.sboxes:
            #        sb.printSbox()

            # est.Est()
            e = est.simpleEstimation(vls, vrs)

            print (int(sol[ind][0]) - 1), e#, solutions[-1]["time"], solutions[-2]["time"]
            #x1.append(int(sol[ind][0]) - 1)
            #x2.append(pow(2, e[2]))
            #x3.append(pow(2, e[1]))
            x.append([math.log(int(sol[ind][0]), 2) - 1, e[2], e[1]])
            ind += 1
            if ind >= len(sol):
                break
            #sol.append(solutions)
            #print key
            #print len(vals) - vals.count("-")
            #print solutions

            #e = ExperimentLauncher("Present_128_SB")
            #s = e.get_solutions(P=140, k=key)
            #s = e.get_solutions(definedbits=[str(v) if type(v) != int else v for v in vals ], k=key)
            #print s
            #print len(s), s[-1]["time"], solutions[-1]["time"]

        elif line[0] == "[":
            #print line
            vals = json.loads(line)
        else:
            key = line[:-1].decode("hex")
    except:
        pass
'''
'''
print stats.ttest_ind(np.array(x1), np.array(x2))
x.sort()
print x
y1 = []
y2 = []
y3 = []
for i in x:
    y1.append(i[0])
    y2.append(i[1])
    y3.append(i[2])


x0 = np.arange(len(x))
y1 = np.array(y1)
y2 = np.array(y2)
y3 = np.array(y3)
#print x0
#print y1


#y_all_ = np.array(y_all_)


plt.plot(x0, y1, '.', label=u"Действительное")
plt.plot(x0, y2, '.', label=u"Предсказанное")
plt.plot(x0, y3, '.', label=u"Максимальное предсказанное")
plt.title(u"Число подходящих ключей")
#plt.xlabel(u"Число подходящих ключей")
#plt.ylabel(u"Число подходящих ключей")
plt.legend(fontsize=14)
plt.show()
'''


t='''2258.4 2200.0 (32, 28, 12) [1800, 2800, 864, 2880, 4800, 2400, 1200, 2400, 1440, 2000]
68.8 48.0 (24, 18, 5) [48, 64, 8, 48, 48, 24, 256, 96, 32, 64]
150.4 136.0 (23, 18, 13) [256, 96, 96, 96, 144, 96, 144, 256, 128, 192]
768.0 512.0 (28, 19, 10) [512, 1024, 512, 512, 512, 512, 2048, 256, 1024, 768]
1062.4 640.0 (27, 17, 12) [512, 2048, 1536, 384, 512, 768, 384, 1024, 384, 3072]
294.4 192.0 (23, 16, 9) [96, 192, 192, 384, 96, 768, 192, 128, 384, 512]
528.0 224.0 (26, 15, 12) [2304, 64, 96, 256, 192, 192, 256, 1536, 256, 128]
1436.8 864.0 (29, 25, 11) [384, 1536, 6912, 160, 1152, 576, 1152, 1920, 384, 192]
21.4 12.0 (24, 13, 7) [4, 72, 16, 12, 64, 12, 8, 12, 8, 6]
12.4 12.0 (16, 11, 9) [16, 8, 16, 16, 24, 8, 12, 12, 8, 4]
62.2 50.0 (21, 13, 9) [30, 120, 60, 96, 60, 120, 40, 36, 30, 30]
108.8 96.0 (21, 17, 6) [256, 96, 128, 32, 256, 32, 32, 128, 96, 32]
78.0 56.0 (23, 16, 14) [256, 4, 72, 24, 48, 48, 144, 96, 64, 24]
710.4 192.0 (25, 20, 11) [192, 1536, 128, 192, 128, 256, 2304, 2048, 192, 128]
1093.6 192.0 (30, 29, 12) [96, 192, 384, 384, 96, 16, 288, 9216, 72, 192]
20.0 14.0 (26, 17, 0) [48, 32, 32, 24, 12, 8, 12, 8, 8, 16]
112.0 32.0 (23, 16, 11) [16, 48, 32, 24, 64, 96, 32, 768, 8, 32]
518.4 384.0 (25, 17, 11) [768, 768, 384, 64, 1152, 768, 384, 128, 384, 384]
22.1 24.0 (24, 15, 6) [16, 36, 32, 16, 6, 9, 32, 6, 36, 32]
28.8 32.0 (25, 21, 10) [32, 64, 32, 16, 32, 8, 32, 32, 32, 8]
6124.8 4128.0 (27, 20, 16) [7680, 2560, 1152, 5120, 3456, 4800, 18432, 15360, 1152, 1536]
103.2 56.0 (27, 21, 3) [64, 32, 48, 64, 32, 64, 128, 8, 16, 576]
954.4 600.0 (26, 19, 17) [144, 2160, 2304, 288, 288, 280, 432, 1152, 1728, 768]
438.0 440.0 (24, 17, 7) [480, 720, 480, 800, 240, 400, 240, 180, 480, 360]
47.1 40.0 (23, 14, 1) [20, 80, 80, 40, 24, 24, 80, 3, 80, 40]
252.0 168.0 (24, 17, 10) [864, 288, 144, 432, 192, 72, 96, 36, 288, 108]
502.4 480.0 (27, 21, 10) [256, 768, 768, 96, 384, 768, 256, 1024, 576, 128]
155.2 76.0 (24, 20, 12) [128, 24, 72, 144, 648, 48, 16, 72, 80, 320]
2771.2 1408.0 (27, 21, 17) [576, 1152, 1280, 1728, 3840, 9600, 6720, 384, 1536, 896]
228.8 112.0 (28, 19, 10) [144, 192, 32, 256, 320, 32, 1152, 48, 32, 80]
100.4 75.0 (24, 21, 12) [192, 24, 96, 192, 54, 216, 54, 32, 48, 96]
233.6 160.0 (23, 17, 4) [64, 1024, 256, 64, 32, 64, 64, 256, 256, 256]
106.4 88.0 (28, 21, 10) [48, 240, 96, 80, 160, 24, 96, 48, 192, 80]
531.2 448.0 (29, 22, 11) [256, 64, 384, 512, 768, 512, 256, 256, 1536, 768]
70.0 36.0 (18, 15, 11) [8, 144, 216, 8, 48, 12, 144, 24, 72, 24]
48.0 34.0 (22, 13, 5) [32, 144, 36, 12, 32, 16, 96, 48, 48, 16]
266.4 168.0 (27, 20, 14) [384, 192, 24, 144, 80, 64, 48, 192, 1024, 512]
3161.6 1792.0 (27, 25, 10) [6912, 1536, 384, 4608, 2048, 1536, 9216, 3072, 1536, 768]
70.4 56.0 (24, 14, 8) [40, 80, 120, 96, 48, 48, 40, 64, 160, 8]
764.8 288.0 (25, 19, 8) [768, 2304, 288, 288, 128, 1920, 1536, 128, 192, 96]
225.6 128.0 (31, 21, 11) [128, 128, 64, 64, 1024, 192, 192, 16, 384, 64]
1155.2 912.0 (32, 25, 12) [800, 256, 3072, 1280, 2304, 1024, 512, 1728, 192, 384]
403.2 256.0 (28, 23, 8) [512, 256, 192, 256, 128, 64, 192, 1024, 1152, 256]
2636.8 1536.0 (30, 22, 11) [1536, 6144, 3072, 1536, 1536, 6912, 3072, 512, 512, 1536]
1704.0 1136.0 (25, 20, 15) [480, 3840, 1920, 640, 4480, 2400, 1120, 240, 1152, 768]
57446.4 38400.0 (32, 27, 14) [6144, 147456, 196608, 49152, 4096, 73728, 27648, 2048, 12288, 55296]'''







#  793.6 512.0 (26, 16, 3) [512, 1024, 1024, 512, 512, 512, 256, 512, 1024, 2048]
#   92.0 84.0 (21, 9, 1) [72, 24, 24, 32, 96, 48, 144, 96, 192, 192]

x = []
t=t.split("\n")
for line in t:
    #print line
    tmeanmed, tother = line.split("(")
    ests, arr = tother.split(")")
    #print [i for i in tmeanmed.split(" ")]
    mean, median = [math.log(float(i), 2) for i in tmeanmed.split(" ")[:-1]]
    max, av, min = [int(i) for i in ests.split(", ")]
    x.append([median, mean, max, av, min, [math.log(i, 2) for i in json.loads(arr)]])

x.sort()
print x
y1 = []
y2 = []
y3 = []
y4 = []
y5 = []
y6 = []
for i in x:
        y1.append(i[0])
        y2.append(i[1])
        y3.append(i[2])
        y4.append(i[3])
        y5.append(i[4])
        y6.append(i[5])

x0 = np.arange(len(x))
y1 = np.array(y1)
y2 = np.array(y2)
y3 = np.array(y3)
y4 = np.array(y4)
y5 = np.array(y5)
y6 = np.array(y6).T




#import numpy as np
#import matplotlib.pyplot as plt
import matplotlib.cbook as cbook

# fake data
#np.random.seed(937)
#data = np.random.lognormal(size=(37, 4), mean=1.5, sigma=1.75)
labels = range(len(x))
print y6
# compute the boxplot stats
stats = cbook.boxplot_stats(y6, labels=labels, bootstrap=10000)
# After we've computed the stats, we can go through and change anything.
# Just to prove it, I'll set the median of each set to the median of all
# the data, and double the means
#for n in range(len(stats)):
#    stats[n]['med'] = np.median(y6)
    #stats[n]['mean'] *= 2

print(stats[0].keys())


    # print x0
    # print y1

    # y_all_ = np.array(y_all_)

#plt
#plt.plot(x0, y1, '.', label=u"Действительное медиана")
#plt.plot(x0, y2, '.', label=u"Действительное среднее")
fig, axes = plt.subplots(nrows=1, ncols=1, figsize=(15, 15))
axes.bxp(stats)
plt.plot(x0, y3, '.', label=u"Простой перебор")
plt.plot(x0, y4, '.', label=u"Верхняя граница для CDCL-решателя")
plt.plot(x0, y5, '.', label=u"Среднее предсказанное")

axes.set_xlabel(u"Наборы известных бит", fontdict={"size":14})
axes.set_ylabel(u"Сложность решения (логарифмическая шкала)", fontdict={"size":14})
axes.set_xticklabels([])

plt.title(u"Сложность перебора ключей для Present-80")
    # plt.xlabel(u"Число подходящих ключей")
    # plt.ylabel(u"Число подходящих ключей")
plt.legend(fontsize=14)
plt.show()