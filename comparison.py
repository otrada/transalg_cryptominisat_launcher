# -*- coding: utf-8 -*-



import json
from Experiments_launcher import ExperimentLauncher


f = open("exxxp_")
sol = []
vals = solutions = key = None
for line in f:
    #print line
    if line[:3] == "EXP":
        print line
        pass

    elif line[:2] == "[{":
        solutions = json.loads(line)
        sol.append(solutions)
        #print key
        #print len(vals) - vals.count("-")
        #print solutions

        #e = ExperimentLauncher("Present_128_SB")
        #s = e.get_solutions(P=140, k=key)
        #s = e.get_solutions(definedbits=[str(v) if type(v) != int else v for v in vals], k=key)
        #print s
        #print len(s), s[-1]["time"], solutions[-1]["time"]

    elif line[0] == "[":
        #print line
        vals = json.loads(line)
    else:
        key = line[:-1].decode("hex")


import matplotlib.pyplot as plt
import numpy as np
ind = 0
x = []
y_keys = []
y_all = []
y_all_ = []
f = open("ex")
for line in f:
    if line[:3] == "EXP":
        print line[:-1]#, sol[ind][-2]["time"]
    else:
        s = line.split(" ")
        print s
        print (int(s[0]) - 1), float(s[1]), float(s[2]), sol[ind][-1]["time"], sol[ind][-2]["time"]
        if float(s[2]) != sol[ind][-1]["time"]:
            ind += 1
        if int(s[0]) < 15000:
            x.append(int(s[0]) - 1)
            y_keys.append(float(s[1]))
            y_all.append(float(s[2]))
            y_all_.append(sol[ind][-2]["time"])
            ind += 1

x = np.array(x)
y_all = np.array(y_all)
y_keys = np.array(y_keys)
y_all_ = np.array(y_all_)


plt.plot(x, y_keys, '.', label=u"Время решения для алгоритма развертывания ключа")
plt.plot(x, y_all, '.', label=u"Время решения для алгоритма шифрования")
plt.plot(x, y_all_, '.', label=u"Время нахождения последнего ключа для алгоритма шифрования")
plt.title("Present-80")
plt.xlabel(u"Число подходящих ключей для алгоритма развертывания ключа")
plt.ylabel(u"Время решения")
plt.legend(fontsize=14)
plt.show()


'''
/home/otrada/Src/PycharmProjects/SAT/venv/bin/python /home/otrada/Src/PycharmProjects/SAT/comparison.py
'''
