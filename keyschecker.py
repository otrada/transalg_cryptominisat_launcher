#*-* coding: utf-8 *-*
import json
import matplotlib.pyplot as plt
import statistics

'''
#f = open("Experiments/keys_SB (copy)")#
f = open("Experiments/keys_SB_")
ft = f.read()
f.close()
print len(ft)

m = 0
ft = ft.replace("}{", "}\n{")
fn = open("Experiments/new_test_keys_SB", "w")
fn.write(ft)
fn.close()
'''

dic = {}
dic_m = {}
dict = {}
dict_m = {}
ar = []
xd = []
yd = []
xm = []
ym = []

xn = []
xt = []

txt = ""
fl = open("Experiments/new_test_keys_SB")
for i in fl:
    j = json.loads(i)
    f = True
    c = 0
    '''
    exp = []
    times = []
    for t in j["exp"]:
        exp.append(len(t) - 1)
        times.append(t[-1]["time"])
        if len(t) >= 10000:
           c += 1
    j["exp"] = exp
    j["times"] = times
    '''
    dic[j["bits"]] = statistics.mean(j["exp"])
    dict[j["bits"]] = statistics.mean(j["times"])
    dic_m[j["bits"]] = statistics.median(j["exp"])
    dict_m[j["bits"]] = statistics.median(j["times"])
    for k in xrange(len(j["exp"])):
        if j["times"][k] < 1500:
            xn.append(j["exp"][k])
            xt.append(j["times"][k])
    #xd.append(j["bits"])
    #yd.append(statistics.mean(j["exp"]))
    print j["bits"], c, len(j["exp"]), statistics.mean(j["exp"]), statistics.median(j["exp"]), \
        max(j["exp"]), min(j["exp"]), statistics.stdev(j["exp"]),\
        statistics.mean(j["times"]), statistics.median(j["times"])
    txt += (json.dumps(j) + "\n")
fl.close()
fl = open("Experiments/new_test_keys_SB", "w")
fl.write(txt)
fl.close()
            #break


def mult(n):
    c = 0
    while n % 2 == 0:
        n /=2
        c += 1
    return c if c > 0 else -1

for key, value in sorted(dic.iteritems(), key=lambda (k,v): (k,v)):
    xd.append(key)
    yd.append(value)
for key, value in sorted(dic_m.iteritems(), key=lambda (k, v): (k, v)):
    xm.append(key)
    ym.append(value)
    #print "%s: %s" % (key, value)


import numpy, math
from scipy.optimize import curve_fit

def func(x, a, b, c, d):
    return a * numpy.exp(-b * x + c) + d

x = numpy.array([xd[i] for i in xrange(len(xd)) if 100 <= xd[i] < 500 and yd[i] < 10000], dtype=float)
y = numpy.array([yd[i] for i in xrange(len(xd)) if 100 <= xd[i] < 500 and yd[i] < 10000], dtype=float)
x_ = numpy.array([xm[i] for i in xrange(len(xm)) if 103 <= xm[i] < 500 and ym[i] < 10000], dtype=float)
y_ = numpy.array([ym[i] for i in xrange(len(xm)) if 103 <= xm[i] < 500 and ym[i] < 10000], dtype=float)

def fnc(x, a, b):
    return a * x + b

xn = numpy.array(xn)
xt = numpy.array(xt)

plt.scatter(xn, xt, edgecolors="b")
popt, pcov = curve_fit(fnc, xn, xt)
print popt, pcov
#for i in [50, 75, 100, 110, 120, 150, 200, 400, 500]:
#    print i, func(i, *popt)

plt.plot(xn, fnc(xn, *popt), 'r-', label='fit: %5.3f * x + %5.3f' % tuple(popt))
plt.title(u"Взаимосвязь между числом ключей и временем решения КНФ")
plt.xlabel(u'Число ключей')
plt.ylabel(u'Время решения КНФ (с)')
plt.legend()
plt.show()






plt.figure(figsize=(50, 100))
plt.subplot(2, 2, 1)
#plt.axes((120, 250, 0, 3000))
plt.plot(x[:-250], y[:-250], 'b-', label=u'Среднее')
popt, pcov = curve_fit(func, x, y, p0=[1.39490098e+02,   1.31368095e-01,   1.93574394e+01,   3.90167124e+00])
print popt, pcov
for i in [40, 50, 75, 100, 110, 120, 150, 200, 400, 500]:
    print i, func(i, *popt)

plt.plot(x[:-250], func(x[:-250], *popt), 'r-', label='fit: %5.3f * exp(- %5.3f * x + %5.3f) + %5.3f' % tuple(popt))
plt.title(u"Число ключей (среднее)")
plt.xlabel(u'Число известных бит')
plt.ylabel(u'Число подходящих ключей')
plt.legend()

plt.subplot(2, 2, 2)
#plt.axes((120, 250, 0, 700))
plt.plot(x_[:-250], y_[:-250], 'b-', label=u'Медиана')
popt_, pcov_ = curve_fit(func, x_, y_, p0=[1.39490098e+02,   1.31368095e-01,   1.93574394e+01,   3.90167124e+00])
print popt_, pcov_
for i in [40, 50, 75, 100, 110, 120, 150, 200, 400, 500]:
    print i, func(i, *popt_)
plt.plot(x_[:-250], func(x_[:-250], *popt_), 'r-', label='fit: %5.3f * exp(- %5.3f * x + %5.3f) + %5.3f' % tuple(popt_))

plt.title(u"Число ключей (медиана)")
plt.xlabel(u'Число известных бит')
plt.ylabel(u'Число подходящих ключей')
plt.legend()
#plt.show()



xd = []
yd = []

xm = []
ym = []
for key, value in sorted(dict.iteritems(), key=lambda (k,v): (k,v)):
    xd.append(key)
    yd.append(value)
for key, value in sorted(dict_m.iteritems(), key=lambda (k, v): (k, v)):
    xm.append(key)
    ym.append(value)
    #print "%s: %s" % (key, value)


import numpy, math
from scipy.optimize import curve_fit

def func(x, a, b, c, d):
    return a * numpy.exp(-b * x + c) + d

x = numpy.array([xd[i] for i in xrange(len(xd)) if 115 <= xd[i] < 500 and yd[i] < 10000], dtype=float)
y = numpy.array([yd[i] for i in xrange(len(xd)) if 115 <= xd[i] < 500 and yd[i] < 10000], dtype=float)
x_ = numpy.array([xm[i] for i in xrange(len(xm)) if 115 <= xm[i] < 500], dtype=float)
y_ = numpy.array([ym[i] for i in xrange(len(xm)) if 115 <= xm[i] < 500], dtype=float)

plt.subplot(2, 2, 3)
plt.plot(x[:-250], y[:-250], 'b-', label=u'Среднее')
#plt.axes((120, 250, 0, 140))   p0=[79.619, 0.142, 19.686, 7.712])
popt, pcov = curve_fit(func, x, y, p0=[1.39490098e+02,   1.31368095e-01,   1.93574394e+01,   3.90167124e+00])
for i in [40, 50, 75, 100, 110, 120, 150, 200, 400, 500]:
    print i, func(i, *popt)
print popt, pcov
plt.plot(x[:-250], func(x[:-250], *popt), 'r-', label='fit: %5.3f * exp(- %5.3f * x + %5.3f) + %5.3f' % tuple(popt))
plt.title(u"Число решения (среднее)")
plt.xlabel(u'Число известных бит')
plt.ylabel(u'Число решения (с)')

plt.legend()

plt.subplot(2, 2, 4)
plt.plot(x_[:-250], y_[:-250], 'b-', label=u'Медиана')
#plt.axis((120, 250, 0, 35))
popt_, pcov_ = curve_fit(func, x_, y_, p0=[1.39490098e+02,   1.31368095e-01,   1.93574394e+01,   3.90167124e+00])
print popt_, pcov_
for i in [40, 50, 75, 100, 110, 120, 150, 200, 400, 500]:
    print i, func(i, *popt_)

plt.plot(x_[:-250], func(x_[:-250], *popt_), 'r-', label='fit: %5.3f * exp(- %5.3f * x + %5.3f) + %5.3f' % tuple(popt_))
plt.title(u"Число решений (медиана)")
plt.xlabel(u'Число известных бит')
plt.ylabel(u'Число решений (с)')

plt.legend()
plt.show()













'''
x = [i for i in dic if len(dic[i]) > 10]
y = [statistics.mean(dic[i]) for i in dic if len(dic[i]) > 10]
print x, y
p = plt.plot(nrows=1, ncols=2, figsize=(12, 5))
plt.scatter(x, y, edgecolors='r', s=10)
'''

'''
bplot = [0] * len(ar)
ar = sorted(ar)


#for v, i in enumerate(ar):
bplot = axes[0].boxplot(a,
                             vert=True,  # vertical box aligmnent
                             patch_artist=True)
    #print i, statistics.mean(dic[i])

'''


'''


import numpy as np

# Random test data
np.random.seed(123)
all_data = [np.random.normal(0, std, 100) for std in range(1, 4)]


# rectangular box plot
bplot1 = axes[0].boxplot(all_data,
                         vert=True,   # vertical box aligmnent
                         patch_artist=True)   # fill with color

# notch shape box plot
bplot2 = axes[1].boxplot(all_data,
                         notch=True,  # notch shape
                         vert=True,   # vertical box aligmnent
                         patch_artist=True)   # fill with color

# fill with colors
colors = ['pink', 'lightblue', 'lightgreen']
for bplot in (bplot1, bplot2):
    for patch, color in zip(bplot['boxes'], colors):
        patch.set_facecolor(color)

# adding horizontal grid lines
for ax in axes:
    ax.yaxis.grid(True)
    ax.set_xticks([y+1 for y in range(len(all_data))], )
    ax.set_xlabel('xlabel')
    ax.set_ylabel('ylabel')

# add x-tick labels
plt.setp(axes, xticks=[y+1 for y in range(len(all_data))],
         xticklabels=['x1', 'x2', 'x3', 'x4'])
'''
plt.show()









'''
for i in f:
    print 7
    try:
        j = json.loads(i)
        f = True
        for t in j["exp"]:
            if len(t) == 10000:
                print j["bits"]
                break
    except:
        pass'''