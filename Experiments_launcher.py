import random
import time
import json

from os import urandom
from Ciphers.Present import Present
from Ciphers.GOST import GOST3412Kuznechik
from Ciphers.LBlock import LBlock
from Ciphers.MIBS import MIBS
from Ciphers.AES import AES
from Ciphers.Rectangle import Rectangle
from DIMACS_parser import DIMACSParser
from Cryptominisat_runner import CryptominisatRunner
from statistics import mean, median, stdev


class ExperimentLauncher:
    def __init__(self, algorithm):
        self.algo = algorithm
        if self.algo == "Present_80" or self.algo == "Present_80_SB":
            self.keylength = 10
            self.cipher = Present
            self.rounds = 32
            self.roundkeylength = 64
            self.outputformat = {
                "type": "in",
                "vars_number": 80,
                "block_size": 80,
                "shift": 1,
                "border": None
            }
        elif self.algo == "Present_128" or self.algo == "Present_128_SB":
            self.keylength = 16
            self.cipher = Present
            self.rounds = 32
            self.roundkeylength = 64
            self.outputformat = {
                "type": "in",
                "vars_number": 128,
                "block_size": 128,
                "shift": 1,
                "border": None
            }
        elif self.algo == "LBlock" or self.algo == "LBlock_SB":
            self.keylength = 10
            self.cipher = LBlock
            self.rounds = 32
            self.roundkeylength = 32
            self.outputformat = {
                "type": "in",
                "vars_number": 80,
                "block_size": 80,
                "shift": 1,
                "border": None
            }
        elif self.algo == "MIBS_80" or self.algo == "MIBS_80_SB":
            self.keylength = 10
            self.cipher = MIBS
            self.rounds = 32
            self.roundkeylength = 32
            self.outputformat = {
                "type": "in",
                "vars_number": 80,
                "block_size": 80,
                "shift": 1,
                "border": None
            }
        elif self.algo == "MIBS_64" or self.algo == "MIBS_64_SB":
            self.keylength = 8
            self.cipher = MIBS
            self.rounds = 32
            self.roundkeylength = 32
            self.outputformat = {
                "type": "in",
                "vars_number": 64,
                "block_size": 64,
                "shift": 1,
                "border": None
            }
        elif self.algo == "AES_128_SB":
            self.keylength = 16
            self.cipher = AES
            self.rounds = 44 * 4
            self.roundkeylength = 8
            self.outputformat = {
                "type": "in",
                "vars_number": 128,
                "block_size": 128,
                "shift": 1,
                "border": None
            }
        elif self.algo == "Rectangle_80_SB":
            self.keylength = 10
            self.cipher = Rectangle
            self.rounds = 26
            self.roundkeylength = 64
            self.outputformat = {
                "type": "in",
                "vars_number": 80,
                "block_size": 80,
                "shift": 1,
                "border": None
            }
        elif self.algo == "Rectangle_128_SB":
            self.keylength = 16
            self.cipher = Rectangle
            self.rounds = 26
            self.roundkeylength = 64
            self.outputformat = {
                "type": "in",
                "vars_number": 128,
                "block_size": 128,
                "shift": 1,
                "border": None
            }

        elif self.algo == "Kuznechik" or self.algo == "Kuznechik_SB":
            self.keylength = 32
            self.cipher = GOST3412Kuznechik
            self.rounds = 10
            self.roundkeylength = 128
            self.outputformat = {
                "type": "in",
                "vars_number": 256,
                "block_size": 256,
                "shift": 1,
                "border": None
            }
    def get_random_part(self, vals, part):
        #print vals, part
        #print len(vals), part
        #print part, len(vals)
        s = random.sample(xrange(len(vals)),  part)

        return self.get_defined_part(vals, s)

    def get_defined_part(self, vals, part):
        #print vals, part
        part = list(part)
        part.sort()
        for i in xrange(len(vals)):
            if i not in part:
                vals[i] = "-"
        return vals

    def get_solutions(self, k=None, P=None, keybits=None, definedbits = None):
        vars = []
        if not k:
            k = urandom(self.keylength)
        print k.encode("hex")
        #print hex(int(k[::-1].encode("hex"), 16)) AES

        p = self.cipher(k, rounds=self.rounds)

        for k in p.roundkeys:
            #print hex(k),
            b = bin(k)[2:].rjust(self.roundkeylength, '0')[::-1]
            for i in b:
                vars.append(int(i))
        #print
        #print len(vars), len(p.roundkeys)
        p = DIMACSParser("%s.cnf" % self.algo)
        #print 111111
        if keybits:
            p.replaceOutput(self.get_defined_part(vars, keybits), "newkeys1.cnf")
        elif definedbits:
            p.replaceOutput(definedbits, "newkeys1.cnf")
            #print "def", definedbits
        else:
            #print P, "P", len(vars), self.get_random_part(vars, P)
            p.replaceOutput(self.get_random_part(vars, P), "newkeys1.cnf")
            #print "rand", self.get_random_part(vars, P)

        #print 111111
        sol = []
        c = CryptominisatRunner("newkeys1.cnf", verbose=0)
        for i in c.runCryptominisat(output=self.outputformat):
            sol.append(i)
            if len(sol) % 10000 == 0:
                print "fff", len(sol), time.time()
            #print i
        #print "rrrrrrrrrrrrrrrrr",
        #for s in sol:
            #print s,
        #    if s["solution"]:
        #        print hex(s["solution"][0]),
            #print hex(s),
        #print
        #print int(k.encode("hex"), 16)
        return sol



    def many_solutions(self, P, n):
        exp = []
        times = []
        keys = []
        #if type(P) == int:
        #    P = 1.0 * P / (self.roundkeylength * self.rounds)
        for i in xrange(n):
            if type(P) == int:
                exp.append(self.get_solutions(P=P))
            else:
                exp.append(self.get_solutions(keybits=P))
            #print exp
            times.append(exp[-1][-1]["time"])
            keys.append(len(exp[-1]) - 1)
        arr = keys
        print max(arr), min(arr), mean(arr), median(arr), stdev(arr)

        return {
            "time.av": mean(times),
            "keys.av": mean(keys),
            "time.stdev": stdev(times),
            "keys.stdev": stdev(keys),
            "exp": exp
        }



    def get_multiple_solutions_for_fixed_bits(self, varnums, n):
        sols = []
        for _ in range(n):
            vars = []
            k = urandom(self.keylength)
            # print hex(int(k.encode("hex"), 16))
            p = self.cipher(k, rounds=self.rounds)
            #print k
            for k in p.roundkeys:
                # print k,
                b = bin(k)[2:].rjust(self.roundkeylength, '0')[::-1]
                for i in b:
                    vars.append(int(i))
            # print
            p = DIMACSParser("%s.cnf" % self.algo)
            # print 111111
            p.replaceOutput(self.get_defined_part(vars, varnums), "newkeys1.cnf")

            # print 111111
            sol = []
            c = CryptominisatRunner("newkeys1.cnf", verbose=0)
            for i in c.runCryptominisat(output=self.outputformat):
                sol.append(i)
            #print sol
            sols.append(len(sol) - 1)
                # print i
            # print "rrrrrrrrrrrrrrrrr",
            # for s in sol:
            #    if s["solution"]:
            #        print hex(s["solution"][0]),
            #    print hex(s),
            # print
            # print int(k.encode("hex"), 16)
        #print sols
        return sols


    def write_statistic(self, bits, n):


        arr = self.many_solutions(bits, n)
        stat = arr
        stat["bits"] = bits
        print bits, stat["keys.av"], stat["keys.stdev"], stat["time.av"], stat["time.stdev"]
        w = open("Experiments/%s/%s" % (self.algo, self.algo), "a")
        w.write(json.dumps(stat) + "\n")
        w.close()

