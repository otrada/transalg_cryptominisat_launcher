# coding: utf-8 -*-
from Transalg_runner import runTransalg
from Experiments_launcher import ExperimentLauncher
from Stats import Stats
import matplotlib.pyplot as plt
from statistics import mean, median
#import numpy as np

#Present_80_SB - 117
#Present_80_SB - 256

name = "Rectangle_128_SB"
name_ = "Rectangle_128_dif_SB"


p_if_x = []
p_if_times_mean = []
p_if_times_medi = []
p_if_keysn_mean = []
p_if_keysn_medi = []


BORDER = 5
keysdict = {}
# BORDER = 180
s = Stats("%s/%s" % (name, name))
s.get_file_stat()
print s.dic
for key in s.dic:
    if s.dic[key]["times"]["mean"] <= BORDER:
        if key in keysdict:
            p_if_keysn_mean.append(mean([s.dic[key]["keysn"]["mean"], keysdict[key]["mean"]]))
            p_if_keysn_medi.append(mean([s.dic[key]["keysn"]["median"], keysdict[key]["median"]]))
        else:
            p_if_keysn_mean.append(s.dic[key]["keysn"]["mean"])
            p_if_keysn_medi.append(s.dic[key]["keysn"]["median"])
        p_if_x.append(key)
        p_if_times_mean.append(s.dic[key]["times"]["mean"])
        p_if_times_medi.append(s.dic[key]["times"]["median"])


        #p_if_x.append(key)
        #keysdict[key] = {
        #    "mean":   s.dic[key]["keysn"]["mean"],
        #    "median": s.dic[key]["keysn"]["median"]}
        #p_if_x.append(key)
        #p_if_times_mean.append(s.dic[key]["times"]["mean"])
        #p_if_times_medi.append(s.dic[key]["times"]["median"])



p_sb_x = []
p_sb_times_mean = []
p_sb_times_medi = []
p_sb_keysn_mean = []
p_sb_keysn_medi = []


t = Stats("%s/%s" % (name_, name_))
t.get_file_stat()
print t.dic
for key in t.dic:
    if s.dic[key]["times"]["mean"] <= BORDER:
        if key in keysdict:
            p_sb_keysn_mean.append(mean([t.dic[key]["keysn"]["mean"], keysdict[key]["mean"]]))
            p_sb_keysn_medi.append(mean([t.dic[key]["keysn"]["median"], keysdict[key]["median"]]))
        else:
            p_sb_keysn_mean.append(t.dic[key]["keysn"]["mean"])
            p_sb_keysn_medi.append(t.dic[key]["keysn"]["median"])
        p_sb_x.append(key)
        p_sb_times_mean.append(t.dic[key]["times"]["mean"])
        p_sb_times_medi.append(t.dic[key]["times"]["median"])

#plt.figure(2, (10, 5))
fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(20, 10), tight_layout=True)

fig.suptitle("Rectangle-128", fontsize=16)
ax1.set_title(u"Время решения")
ax1.plot(p_if_x, p_if_times_mean, '.', label=u"Среднее для кодировки \nс S-бокс Rectangle")
ax1.plot(p_if_x, p_if_times_medi, '.', label=u"Медиана для кодировки \nс S-бокс Rectangle")
ax1.plot(p_sb_x, p_sb_times_mean, '.', label=u"Среднее для кодировки \nс другой S-бокс")
ax1.plot(p_sb_x, p_sb_times_medi, '.', label=u"Медиана для кодировки \nс другой S-бокс")
ax1.set_xlabel(u"Число известных бит")
ax1.set_ylabel(u"Время решения")
ax1.legend(fontsize=14)

#plt.subplot(2, 2)
ax2.set_title(u"Число ключей")
ax2.plot(p_if_x, p_if_keysn_mean, '.', label=u"Среднее для кодировки \nс S-бокс Rectangle")
ax2.plot(p_if_x, p_if_keysn_medi, '.', label=u"Медиана для кодировки \nс S-бокс Rectangle")
ax2.plot(p_sb_x, p_sb_keysn_mean, '.', label=u"Среднее для кодировки \nс другой S-бокс")
ax2.plot(p_sb_x, p_sb_keysn_medi, '.', label=u"Медиана для кодировки \nс другой S-бокс")
ax2.legend(fontsize=14)
ax2.set_xlabel(u"Число известных бит")
ax2.set_ylabel(u"Число подходящих ключей")
plt.show()








