# coding: utf-8 -*-
from Transalg_runner import runTransalg
from Experiments_launcher import ExperimentLauncher
from Stats import Stats
import matplotlib.pyplot as plt
from statistics import mean, median
#import numpy as np

#Present_80_SB - 117
#Present_128_SB - 256

name = "Present_128_SB"


runTransalg(32, 1, name)

e = ExperimentLauncher(name)
for i in xrange(255, 99, -1):
    if i < 120:
        n = 10
    if i <= 130:
        n = 20
    if i <= 150:
        n = 50
    elif i <= 200:
        n = 100
    elif i <= 300:
        n = 100
    else:
        n = 1000
    print i
    e.write_statistic(i, n)
    #e.write_statistic(501, 100)


p_if_x = []
p_if_times_mean = []
p_if_times_medi = []
p_if_keysn_mean = []
p_if_keysn_medi = []

p_sb_x = []
p_sb_times_mean = []
p_sb_times_medi = []
p_sb_keysn_mean = []
p_sb_keysn_medi = []
keysdict = {}

BORDER = 400
# BORDER = 180
s = Stats("Present_128/Present_128")
s.get_file_stat()
print s.dic
for key in s.dic:
    if key <= BORDER:
        p_if_x.append(key)
        keysdict[key] = {
            "mean":   s.dic[key]["keysn"]["mean"],
            "median": s.dic[key]["keysn"]["median"]}
        p_if_times_mean.append(s.dic[key]["times"]["mean"])
        p_if_times_medi.append(s.dic[key]["times"]["median"])

t = Stats("Present_128_SB/Present_128_SB")
t.get_file_stat()
print t.dic
for key in s.dic:
    if key <= BORDER:
        if key in keysdict:
            p_sb_keysn_mean.append(mean([t.dic[key]["keysn"]["mean"], keysdict[key]["mean"]]))
            p_sb_keysn_medi.append(mean([t.dic[key]["keysn"]["median"], keysdict[key]["median"]]))
        else:
            p_sb_keysn_mean.append(t.dic[key]["keysn"]["mean"])
            p_sb_keysn_medi.append(t.dic[key]["keysn"]["median"])
        p_sb_x.append(key)
        p_sb_times_mean.append(t.dic[key]["times"]["mean"])
        p_sb_times_medi.append(t.dic[key]["times"]["median"])

#plt.figure(2, (10, 5))
fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(20, 10), tight_layout=True)

fig.suptitle("Present-128", fontsize=16)
ax1.set_title(u"Время решения")
ax1.plot(p_if_x, p_if_times_mean, '.', label=u"Среднее для кодировки \nс использованием условных операторов")
ax1.plot(p_if_x, p_if_times_medi, '.', label=u"Медиана для кодировки \nс использованием условных операторов")
ax1.plot(p_sb_x, p_sb_times_mean, '.', label=u"Среднее для кодировки \nс использованием полиномов Жегалкина")
ax1.plot(p_sb_x, p_sb_times_medi, '.', label=u"Медиана для кодировки \nс использованием полиномов Жегалкина")
ax1.set_xlabel(u"Число известных бит")
ax1.set_ylabel(u"Время решения")
ax1.legend(fontsize=14)

#plt.subplot(2, 2)
ax2.set_title(u"Число ключей")
ax2.plot(p_if_x, p_sb_keysn_mean, '.', label=u"Среднее")
ax2.plot(p_if_x, p_sb_keysn_medi, '.', label=u"Медиана")
ax2.legend(fontsize=14)
ax2.set_xlabel(u"Число известных бит")
ax2.set_ylabel(u"Число подходящих ключей")
plt.show()




#ERROR: memory manager can't handle the load. **PLEASE RECOMPILE WITH -DLARGEMEM=ON** size: 1073740882 needed: 1598 newcapacity: 1073741823
#terminate called after throwing an instance of 'std::bad_alloc'
#  what():  std::bad_alloc,






