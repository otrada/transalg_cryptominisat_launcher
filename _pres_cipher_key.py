from Ciphers.Present import Present
from DIMACS_parser import DIMACSParser
from Cryptominisat_runner import CryptominisatRunner
from os import urandom, path
from Transalg_runner import runTransalg
from Experiments_launcher import ExperimentLauncher
import random, json, statistics
import time
import numpy as np


class Hexstring(str):
    def __xor__(self, other):
        return hex(int(self.encode("hex"), 16) ^ int(other.encode("hex"), 16)).decode("hex")

    def diff(self, other):
        return hex(int(self.encode("hex"), 16) ^ int(other, 16))[2:-1].rjust(16, "0").decode("hex")

class Experiment(ExperimentLauncher):
    def __init__(self):
        ExperimentLauncher.__init__(self, "Present_80_SB")
        self.data = []
        #self.title = "Present_80_SB.cnf"
        #self.newtitle = "newkeys1.cnf"

    def exp(self, keybits, P=None):
        vars = []

        KEY = urandom(self.keylength)
        print hex(int(KEY.encode("hex"), 16))
        KEY = "d05b5b04b815c3cb6b5a".decode("hex")
        print hex(int(KEY.encode("hex"), 16))
        p = self.cipher(KEY, rounds=self.rounds)

        for k in p.roundkeys:
            b = bin(k)[2:].rjust(self.roundkeylength, '0')[::-1]
            for i in b:
                vars.append(int(i))

        p = DIMACSParser("%s.cnf" % self.algo)
        if not P:
            p.replaceOutput(self.get_defined_part(vars, keybits), "newkeys1.cnf")
        else:
            p.replaceOutput(self.get_random_part(vars, P), "newkeys1.cnf")

        c = CryptominisatRunner("newkeys1.cnf", verbose=0)
        for i, sol in enumerate(c.runCryptominisat(output=self.outputformat)):
            self.data.append(sol)
            if int(KEY.encode("hex"), 16) == sol["solution"][0]:
                print "FFFOOOUUUNNNDDD"
            if len(self.data) == 10000:
                print i, sol
                w = open("tests/Present_80_SB/file1", "a")
                for t in self.data:
                    w.write(json.dumps(t) + "\n")
                w.close()
                self.data = []
        w = open("tests/Present_80_SB/file1", "a")
        for t in self.data:
            w.write(json.dumps(t) + "\n")
        w.close()
        print self.data[-1]
        self.data = []



    '''
    def gen_cnf(self, ot, ct, k):
        start = "".join([bin(int(o.encode("hex"), 16))[2:].rjust(64, '0')[::-1] for o in ot])
        end = "".join([bin(int(c.encode("hex"), 16))[2:].rjust(64, '0')[::-1] for c in ct])
        varss = [int(i) for i in start] + ["-"] * 80
        #print k
        #print getKnownPart(k, [6, 8, 10, 12])
        #print getRandomPart(k, 0.1)
        #print getKnownPart(k, [6, 8, 10, 12])
        #print len(getRandomPart(k, 1))
        #print 64 * rounds
        #print len(end)
        #printgetRandomPart(k, 0.1) +
        varse = k + [int(i) for i in end]
        #varse = [0] * 64
        #print len(varse)
        p = DIMACSParser(self.title)
        p.replaceInput(varss, self.newtitle)
        p.replaceOutput(varse, self.newtitle)

    def get_data(self):
        min_times = []
        max_times = []
        for i in self.data:
            min_times.append(i["exp"]["time"])
           # max_times.append(i["max_time"])
        txt = {
            "rounds": self.rounds,
            "blocks": self.blocks,
            "exp": self.data,
            "av_min_time": statistics.mean(min_times),
#            "sd_min_time": statistics.stdev(min_times)
        #    "av_max_time": statistics.mean(max_times),
        #    "sd_max_time": statistics.stdev(max_times)
        }
        print
        print self.rounds, self.blocks, \
            txt["av_min_time"],time.time() #txt["sd_min_time"],
        print
        #    txt["av_max_time"], txt["sd_max_time"]
        w = open("tests/Present_80/file1", "a")
        w.write(json.dumps(txt) + "\n")
        w.close()
        '''


def getRandomPart(vals, part):
    s = random.sample(xrange(len(vals)), int(len(vals) * (1.0 - part)))
    s.sort()
    for i in s:
        vals[i] = "-"
    return vals
'''
def getKnownPart(vals, part):
    part.sort()
    for i in part:
        vals[i] = "-"
    return vals
'''
def getKnownPart(vals, bits):
    for i in xrange(len(vals)):
        if i not in bits:
            vals[i] = "-"
    return vals

#tl = "Present"
for rounds in xrange(17, 18):
        vals1 = np.array([4, 6, 8, 10, 12, 14, 20, 22, 24, 26, 28, 30,
                         36, 38, 40, 42, 44, 46, 52, 54, 56, 58, 60, 62])
        vals1 += 64 * (rounds - 1)
        vals0 = np.array([0, 2, 8, 10, 16, 18, 24, 26, 32, 34, 40, 42, 48, 50, 56, 58,     15, 16, 17])
        vals0 += 64 * (rounds - 2)
        valsres = list(vals0) + list(vals1)
        #print "v, ", vals0, "s", vals1, "t", valsres
        e = Experiment()
        e.exp(keybits=valsres)
        #vals = np.array([3, 4, 5])
        #print vals1

        #e = ExperimentLauncher(rounds, 1, part=valsres)



