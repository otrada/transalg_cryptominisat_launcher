# coding: utf-8 -*-

import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from Stats import Stats
import numpy as np

#axes = [plt.subplot2grid(shape=(2,6), loc=(0,0), colspan=2),
#plt.subplot2grid((2,6), (0,2), colspan=2),
#plt.subplot2grid((2,6), (0,4), colspan=2),
#plt.subplot2grid((2,6), (1,1), colspan=2),
#plt.subplot2grid((2,6), (1,3), colspan=2)]

#params = [[0.3, 0.1, 20, 0.1], [0.3, 0.1, 20, 0.1], [200, 0.2, 20, 4], [200, 0.2, 20, 4], [200, 0.2, 20, 4]]
keylen = [64 * 32, 64 * 32, 32 * 32, 32 * 32, 32 * 32, 64 * 26, 64 * 26, 128 * 11]
keylen = [80, 128, 80, 64, 80, 80, 128, 128]

def f(x, a, b, c, d):
    return a * np.exp(-b * x + c) + d


#plt.suptitle(u"Зависимость времени решения от относительного количества известных бит \n"
#                 u"раундовых ключей", fontsize=16)

plt.suptitle(u"Зависимость времени решения от относительного количества известных бит \n"
                 u"раундовых ключей", fontsize=16)
for i, name in enumerate(["Present_80", "Present_128", "LBlock", "MIBS_64", "MIBS_80", "Rectangle_80", "Rectangle_128",
                          "AES_128"]):

    p_sb_times_medi = []
    p_sb_x = []
    keysdict = {}

    BORDER = 0.2
    BORDER = 3
    # BORDER = 180
    s = Stats("%s_SB/%s_SB_new" % (name, name))
    s.get_file_stat()
    print s.dic
    for key in s.dic:
        if 1.0 * key / keylen[i] <= BORDER and s.dic[key]["times"]["median"] < 15:
            p_sb_x.append(1.0 * key / keylen[i])
            p_sb_times_medi.append(s.dic[key]["times"]["median"])
    #print p_sb_x
    #print p_sb_times_medi

    #x = np.arange(min(p_sb_x), max(p_sb_x) + 1, 1)
    #ab, cur
    #ab, c = curve_fit(f, p_sb_x, p_sb_times_medi, p0=params[i])
    #p0=[200, 0.2, 20, 4]
    #a, b, c, d = ab
    #print a, b, c, d
    #y = f(x, a, b, c, d)


    #plt.suptitle(u"Зависимость времени решения от числа известных бит \n"
    #             u"при кодировании S-box с помощью полиномов Жегалкина", fontsize=16)
    #axes[i].set_title(name)
    #plt.plot(x, y, label=r'$y = {0} * \exp(-{1}x + {2}) + {3}$'.format(round(a, 3), round(b, 3), round(c, 3),
    #                                                                   round(d, 3)))
    plt.plot(p_sb_x, p_sb_times_medi, '.', label=name)
    #ax1.plot(p_if_x, p_if_times_medi, '.', label=u"Медиана для кодировки \nс использованием условных операторов")
    #ax1.plot(p_sb_x, p_sb_times_mean, '.', label=u"Среднее для кодировки \nс использованием полиномов Жегалкина")
    #ax1.plot(p_sb_x, p_sb_times_medi, '.', label=u"Медиана для кодировки \nс использованием полиномов Жегалкина")
    #axes[i].set_xlabel(u"Число известных бит")
    #axes[i].set_ylabel(u"Время решения")

plt.legend()
plt.show()


'''


'''
