# coding: utf-8 -*-
from Transalg_runner import runTransalg
from Experiments_launcher import ExperimentLauncher
from Stats import Stats
import matplotlib.pyplot as plt
from statistics import mean, median
import numpy as np

vals3 = np.array(range(20, 24))
vals2 = np.array(range(8, 12) + range(28, 32))
vals21 = np.array(range(0, 4) + range(8, 32))
vals20 = np.array(range(0, 4) + range(12, 16) + range(20, 24))
vals19 = np.array(range(0, 4) + range(28, 32))
vals1 = np.array(range(0, 8) + range(12, 16) + range(24, 28))

vals3 += 32 * 2
vals2 += 32 * 1
vals21 += 32 * 20
vals20 += 32 * 19
vals19 += 32 * 18
valsres = list(vals3) + list(vals2) + list(vals21) + list(vals20) + list(vals19) + list(vals1)

name = "LBlock"

e = ExperimentLauncher(name)

print e.get_multiple_solutions_for_fixed_bits(valsres, 1)
#s = Stats("LBlock/" + name)
#s.get_file_stat()
#print s.dic

