from Ciphers.Present import Present
from DIMACS_parser import DIMACSParser
# from cryptominisat_runner import CryptominisatRunner
# from cryptominisat_runner_all import AllSolutions

P = 1.9

import random


def getRandomPart(vals, part):
    s = random.sample(range(len(vals)), int(len(vals) * (1.0 - part)))
    s.sort()
    print len(vals) - len(s)
    for i in s:
        vals[i] = "-"
    return vals

'''
class Experiment:
    def __init__(self, result=False, time=False):
        self.result = result
        self.time = time
'''

vr = []
p = Present("1212121212")

for k in p.roundkeys:
    b = bin(k)[2:].rjust(64, '0')[::-1]
    for i in b:
        vr.append(int(i))

p = DIMACSParser("pres_all.cnf")
rand = getRandomPart(vr, P)
p.replaceOutput(rand, "newpres.cnf")

# a = AllSolutions("newpres.cnf", )
# sol = a.runSolver()
# for i in sol:
#    print i[0]
# c = CryptominisatParser("newpres.cnf", verbose=0)
# c.runCryptominisat()
# print c.presentInputVars(80)


f = open("test_all_solutions_present_keys_%s" % P)
for i in f:
    print bin(int(i, 10))[2:].rjust(80, "0")
print bin(232321839814246430552370)[2:].rjust(80, "0")


'''
#  replace output
vars = []
p = Present_80("1212121212")
for k in p.roundkeys:
    b = bin(k)[2:].rjust(64, '0')[::-1]
    for i in b:
        vars.append(int(i))

print int(bin(int("1212121212".encode("hex"), 16)), 2)

p = DIMACSParser("pres_all.cnf")
p.replaceOutput(getRandomPart(vars, 0.09), "newpres.cnf")

c = CryptominisatParser("newpres.cnf", verbose=0)
c.runCryptominisat()
print c.presentInputVars(80)
'''


'''
#  replace output
vars = []
p = Present_80("1212121212")
for k in p.roundkeys:
    b = bin(k)[2:].rjust(64, '0')[::-1]
    for i in b:
        vars.append(int(i))

print int(bin(int("1212121212".encode("hex"), 16)), 2)

p = DIMACSParser("pres_all.cnf")
p.replaceOutput(vars, "newpres.cnf")

c = CryptominisatParser("newpres.cnf", verbose=0)
print c.presentInputVars(80)
'''


'''
#  replace input

vars = []
k = bin(int("1212121212".encode("hex"), 16))[2:].rjust(80, "0")[::-1]
print k
for i in k:
    vars.append(int(i))

p = DIMACSParser("pres_all.cnf")
p.replaceInput(vars, "newpres.cnf")

c = CryptominisatParser("newpres.cnf", verbose=0)
print c.output
print c.presentOutputVars(2048, 64)

vars = []
p = Present_80("1212121212")
for k in p.roundkeys:
    b = bin(k)[2:].rjust(64, '0')[::-1]
    print b
    for i in b:
        vars.append(int(i))
print vars
'''