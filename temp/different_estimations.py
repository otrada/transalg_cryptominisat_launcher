from Estimator import Estimator, Config
import numpy as np
'''
rounds = 17
vals1 = np.array([4, 6, 8, 10, 12, 14, 20, 22, 24, 26, 28, 30,
                 36, 38, 40, 42, 44, 46, 52, 54, 56, 58, 60, 62])
vals1 += 64 * (rounds - 1)
vals0 = np.array([0, 2, 8, 10, 16, 18, 24, 26, 32, 34, 40, 42, 48, 50, 56, 58])
vals0 += 64 * (rounds - 2)
vals2 = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 63, 62, 61, 60, 59, 58, 57, 56, 55, 54, 53, 52]
valsres = list(vals0) + list(vals1) + vals2

est = Estimator(valsres, ["d"] * len(valsres), Config("Present_80"))
print est.simpleEstimation()

est = Estimator(valsres, ["d"] * len(valsres), Config("Present_128"))
print est.simpleEstimation()
'''

vals1 = np.array((2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 35, 36, 37, 38, 39, 40, 41, 42, 44, 43, 45, 46, 47, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63))
vals1 += 64 * 31
vals0 = np.array((6, 22, 38, 54))
vals0 += 64 * 30
valsres = list(vals0) + list(vals1)
est = Estimator(valsres, ["d"] * len(valsres), Config("Present_80"))
t = est.simpleEstimation()
print t
#est.printSboxes()
#tm = [283, 167]
#est.fixVars(tm, ["d"] * 73)
#print est.simpleEstimation()
#est.printSboxes()
#print est.presentRegstates(False)
for i in range(1):
    v = est.not_fullnumbers()
    est.fixVars([v], ["d"] * 3)
    t = est.simpleEstimation()
    print t
print t

'''
#LBlock
vals1 = np.array(range(4, 12) + range(28, 32))
vals2 = np.array(range(4, 8) + range(28, 32))
vals3 = np.array(range(28, 32))
vals16 = np.array(range(12, 16))
vals17 = np.array(range(12, 16))
vals18 = np.array(range(12, 16))
vals19 = np.array(range(12, 16) + range(20, 28))

vals2 += 32 * 1
vals3 += 32 * 2
vals16 += 32 * 15
vals17 += 32 * 16
vals18 += 32 * 17
vals19 += 32 * 18
valsres = list(vals1) + list(vals2) + list(vals3) + list(vals16) + list(vals17) + list(vals18) + list(vals19)

est = Estimator(valsres, ["d"] * len(valsres), Config("LBlock"))
t = est.simpleEstimation()
print t
#est.presentRegstates(True)
for i in range(4):
    v = est.not_fullnumbers()
    est.fixVars([v], ["d"] * 3)
    t = est.simpleEstimation()
    print t
'''

'''
#LBlock
vals3 = np.array(range(20, 24))
vals2 = np.array(range(8, 12) + range(28, 32))
vals21 = np.array(range(0, 4) + range(8, 32))
vals20 = np.array(range(0, 4) + range(12, 16) + range(20, 24))
vals19 = np.array(range(0, 4) + range(28, 32))
vals1 = np.array(range(0, 8) + range(12, 16) + range(24, 28))

vals3 += 32 * 2
vals2 += 32 * 1
vals21 += 32 * 20
vals20 += 32 * 19
vals19 += 32 * 18
valsres = list(vals3) + list(vals2) + list(vals21) + list(vals20) + list(vals19) + list(vals1)

est = Estimator(valsres, ["d"] * len(valsres), Config("LBlock"))
t = est.simpleEstimation()
print t
#est.presentRegstates(True)
for i in range(19):
    v = est.not_fullnumbers()
    est.fixVars([v], ["d"] * 3)
    t = est.simpleEstimation()
    print t
'''


'''
#MIBS GREAT
vals1 = np.array(range(12, 32))
vals0 = np.array(range(20, 28))

vals0 += 32 * 14
valsres = list(vals0) + list(vals1)

est = Estimator(valsres, ["d"] * len(valsres), Config("MIBS_64"))
t = est.simpleEstimation()
print t
#est.presentRegstates(True)
for i in range(3):
    v = est.not_fullnumbers()
    est.fixVars([v], ["d"] * 3)
    t = est.simpleEstimation()
    print t

#K1,1, K1,6, K17,1, K17,6
'''

'''
#MIBS GREAT
vals1 = np.array((1, 2, 3, 4, 21, 22, 23, 24)) - 1
vals0 = np.array((1, 2, 3, 4, 21, 22, 23, 24)) - 1
vals0 += 32 * 16
valsres = list(vals0) + list(vals1)

est = Estimator(valsres, ["d"] * len(valsres), Config("MIBS_64"))
t = est.simpleEstimation()
print t

for i in range(3):
    v = est.not_fullnumbers()
    est.fixVars([v], ["d"] * 3)
    t = est.simpleEstimation()
    print t

#K1,1, K1,6, K17,1, K17,6
'''



'''

est.fixVars([356, 358, 359, 344, 345, 349, 350, 351, 336, 337], ["d"] * 10)
t = est.simpleEstimation()
print t

v = est.not_fullnumbers()
est.fixVars([v], ["d"] * 3)
t = est.simpleEstimation()
print t

est.fixVars([308, 309, 310, 296, 297, 288, 289, 268, 269, 256, 258, 259, 115, 116, 249, 228, 230, 231, 220, 221, 222,
             ], ["d"] * 21)
t = est.simpleEstimation()
print t

for i in range(2):
    v = est.not_fullnumbers()
    est.fixVars([v], ["d"] * 3)
    t = est.simpleEstimation()
    print t

est.fixVars([208, 209, 172, 173, 174 ], ["d"] * 5)
t = est.simpleEstimation()
print t

for i in range(3):
    v = est.not_fullnumbers()
    est.fixVars([v], ["d"] * 3)
    t = est.simpleEstimation()
    print t

est.fixVars([156, 157, 158 ], ["d"] * 3)
t = est.simpleEstimation()
print t

for i in range(3):
    v = est.not_fullnumbers()
    est.fixVars([v], ["d"] * 3)
    t = est.simpleEstimation()
    print t

est.fixVars([20, 21, 22, 23, 24, 25, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 117, 118, 119, 120,
             122, 121], ["d"] * 22)
t = est.simpleEstimation()
print t



est.presentRegstates()
#est.printSboxes()
v = est.not_fullnumbers()
'''
'''
#print t
while (t[-1] > 0):
    t = est.simpleEstimation_hard()
    print t
for s in est.sboxes:
    s.printSbox()

est = Estimator(valsres, ["d"] * len(valsres), Config("Present_128"))
print est.simpleEstimation()'''