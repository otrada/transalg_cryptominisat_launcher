from Ciphers.Present import Present
from DIMACS_parser import DIMACSParser
from Cryptominisat_runner import CryptominisatRunner
from os import urandom, path
from Transalg_runner import runTransalg
import random, json, statistics
import time

class Hexstring(str):
    def __xor__(self, other):
        return hex(int(self.encode("hex"), 16) ^ int(other.encode("hex"), 16)).decode("hex")

    def diff(self, other):
        return hex(int(self.encode("hex"), 16) ^ int(other, 16))[2:-1].rjust(16, "0").decode("hex")

class ExperimentLauncher():
    def __init__(self, rounds, blocks, repeat=None):
        self.data = []
        self.rounds = rounds
        self.blocks = blocks
        self.title = "Transalg/%s/pres_%d_%d.cnf" % (tl, rounds, blocks)
        self.newtitle = "Transalg/%s_/pres_%d_%d.cnf" % (tl, rounds, blocks)
        self.outp = {
                "type": "out",
                "vars_number": 64 * (rounds + blocks),
                "block_size": 64,
                "shift": 64,
                "border": {
                    "shiftl": -blocks,
                    "shiftr": None,
                    "content": [0] * blocks
                }
            }
        if self.rounds == 1:
            repeat = 1000
        elif self.rounds == 2:
            repeat = 500
        elif self.rounds == 3:
            repeat = 250
        else:
            repeat = 30
        print repeat
        for i in xrange(repeat):
            k = urandom(10)
            p = Present(k, rounds=rounds)
            ot = [urandom(8) for i in xrange(blocks)]
            ct = p.encrypt(ot)

            if not (path.isfile(self.title)):
                runTransalg(rounds, blocks, ifile=tl)
            self.gen_cnf(ot, ct)
            c = CryptominisatRunner(self.newtitle, verbose=0)
            sl = None
            for sol in c.runCryptominisat(output=self.outp):
                sl = sol
                #print sl/home/otrada/Src/PycharmProjects/Magistr/SAT/Present_80/tests/cipher/Present_80...
                try:
                    c.proc.terminate()
                except:
                    pass
                break
                #if not sol["SAT"]:
                #    break
            self.data.append({
                "key": k.encode("hex"),
                "plain": [o.encode("hex") for o in ot],
                "cipher": [c.encode("hex") for c in ct],
                "exp": sl
               # "min_time": sol["time"],
               # "max_time": exp[-1]["time"]
            })
        self.get_data()


    def gen_cnf(self, ot, ct):
        start = "".join([bin(int(o.encode("hex"), 16))[2:].rjust(64, '0')[::-1] for o in ot])
        end = "".join([bin(int(c.encode("hex"), 16))[2:].rjust(64, '0')[::-1] for c in ct])
        varss = [int(i) for i in start] + ["-"] * 80
        varse = [int(i) for i in end]

        p = DIMACSParser(self.title)
        p.replaceInput(varss, self.newtitle)
        p.replaceOutput(varse, self.newtitle)

    def get_data(self):
        min_times = []
        max_times = []
        for i in self.data:
            min_times.append(i["exp"]["time"])
           # max_times.append(i["max_time"])
        txt = {
            "rounds": self.rounds,
            "blocks": self.blocks,
            "exp": self.data,
            "av_min_time": statistics.mean(min_times),
            "sd_min_time": statistics.stdev(min_times)
        #    "av_max_time": statistics.mean(max_times),
        #    "sd_max_time": statistics.stdev(max_times)
        }
        print self.rounds, self.blocks, \
            txt["av_min_time"], txt["sd_min_time"], time.time()
        #    txt["av_max_time"], txt["sd_max_time"]
        w = open("tests/cipher/%s" % tl, "a")
        w.write(json.dumps(txt) + "\n")
        w.close()


def getRandomPart(vals, part):
    s = random.sample(xrange(len(vals)), int(len(vals) * (1.0 - part)))
    s.sort()
    for i in s:
        vals[i] = "-"
    return vals

#tl = "Present"
for rounds in xrange(8, 9):
    for blocks in xrange(1, 16):
        e = ExperimentLauncher(rounds, blocks)



'''
/usr/bin/python2.7 /home/otrada/Src/PycharmProjects/Magistr/SAT/Present_80/_pres_cipher.py
30
4 1 730.191382667 921.935566914 1529458358.04
30
Transalg/Present_80.ta
Transalg/Present_80/pres_4_2.cnf
4 2 1124.73759913 1615.16347003 1529492121.06
30
Transalg/Present_80.ta
Transalg/Present_80/pres_4_3.cnf
4 3 44.2976915 28.7644773786 1529493480.75
30
Transalg/Present_80.ta
Transalg/Present_80/pres_4_4.cnf
4 4 19.2121369333 8.94177174769 1529494098.11
30
Transalg/Present_80.ta
Transalg/Present_80/pres_4_5.cnf
4 5 20.5250532 9.14371551293 1529494765.18
30
Transalg/Present_80.ta
Transalg/Present_80/pres_4_6.cnf
4 6 16.5110079333 10.3375821912 1529495322.15
30
Transalg/Present_80.ta
Transalg/Present_80/pres_4_7.cnf
4 7 17.1314622 10.3564806097 1529495908.33
30
Transalg/Present_80.ta
Transalg/Present_80/pres_4_8.cnf
4 8 12.0064892 9.96074705733 1529496351.01
30
Transalg/Present_80.ta
Transalg/Present_80/pres_4_9.cnf
4 9 12.4180272333 9.48973632721 1529496816.53
30
Transalg/Present_80.ta
Transalg/Present_80/pres_4_10.cnf
4 10 11.5686482667 8.63493163369 1529497266.92
30
Transalg/Present_80.ta
Transalg/Present_80/pres_4_11.cnf
4 11 13.8632700333 10.3718329094 1529497796.66
30
Transalg/Present_80.ta
Transalg/Present_80/pres_4_12.cnf
4 12 10.2324523333 7.22664755209 1529498227.85
30
Transalg/Present_80.ta
Transalg/Present_80/pres_4_13.cnf
4 13 11.5200306 11.2987770721 1529498707.84
30
Transalg/Present_80.ta
Transalg/Present_80/pres_4_14.cnf
4 14 16.7195881333 14.5584726866 1529499355.14
30
Transalg/Present_80.ta
Transalg/Present_80/pres_4_15.cnf
4 15 12.5735603667 12.3744052888 1529499888.26

'''


'''
class Experiment:
    def __init__(self, result=False, time=False):
        self.result = result
        self.time = time
'''







'''
b = bin(int((ot).encode("hex"), 16))[2:].rjust(64, '0')[::-1] + \
    bin(int((k).encode("hex"), 16))[2:].rjust(80, '0')[::-1]
vars = [int(i) for i in b]

p = DIMACSParser("pres_all.cnf")
p.replaceInput(vars, "newpres_all.cnf")

a = AllSolutions("newpres_all.cnf")
sol = a.runSolver(outp_type="out", outp_vars=(rounds + 1) * 64, outp_block=64)
print "CIPHEREXP", hex(sol[0][rounds])
print "SOLUTION", [hex(i) for i in sol[0]]
'''

'''
c = CryptominisatParser("newpres_all.cnf", verbose=0)
c.runCryptominisat()
res = c.presentOutputVars(64 * 3, 64)
t = "".join(str(i) for i in c.values)[144:]
print hex(res[2])
print len(t), t
print res if res else "UNSAT"
'''
'''
f = open("test_all_solutions_present_keys_0.06")
for i in f:
    print bin(int(i, 10))[2:].rjust(80, "0")
print bin(232321839814246430552370)[2:].rjust(80, "0")
'''