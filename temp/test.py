# -*- coding:utf-8 -*-
import numpy
import statistics
import json
import matplotlib.pyplot as plt





f1 = open("/home/otrada/Src/PycharmProjects/Magistr/SAT/Present_80/tests/cipher/Present_80")
f2 = open("/home/otrada/Src/PycharmProjects/Magistr/SAT/Present_80/tests/cipher/Present_80_SB")

pr = []
prs = []
c = -1
for i, v in enumerate(f1):
    if i % 15 == 0:
        pr.append([[], []])
        c += 1
    js = json.loads(v)
    print i, pr
    pr[c][0].append(js["blocks"])
    pr[c][1].append(js["av_min_time"])

c = -1
for i, v in enumerate(f2):
    if i % 15 == 0:
        prs.append([[], []])
        c += 1
    js = json.loads(v)
    print i, pr
    prs[c][0].append(js["blocks"])
    prs[c][1].append(js["av_min_time"])

f1.close()
f2.close()

plt.figure(figsize=(50, 100))
plt.title(u"Время решения в зависимости от количества раундов и блоков")
for ctr in xrange(1, 5):
    plt.subplot(2, 2, (ctr))
    #print pr[ctr - 1]["rounds"], js["blocks"], js["av_min_time"], js["sd_min_time"]
    plt.plot(pr[ctr - 1][0], pr[ctr - 1][1], 'b-', label=u'%d раунда, кодировка с условными переходами' % (ctr))
    #jt = json.loads(j)
    plt.plot(prs[ctr - 1][0], prs[ctr - 1][1], 'r-', label=u'%d раунда, кодировка S-box' % (ctr))

    plt.title(u'%d раунда' % ctr)
    plt.xlabel(u'Число блоков')
    plt.ylabel(u'Время решения (с)')
    plt.legend()

plt.show()



arr = [ 960, 960, 576, 1280, 1920, 3840, 640, 2560, 1152, 1152, 1152, 640, 3072, 1280, 768,
 192, 1152, 1920, 1280, 384, 2304, 1280, 1280, 768, 768, 1920, 1280, 2560, 1280,
 2560, 768, 2304]

print max(arr), min(arr), statistics.mean(arr), statistics.median(arr), statistics.stdev(arr)

def func(x, a, b, c, d):
    return a * numpy.exp(-b * x + c) + d

print func(50, 10.526,0.103, 18.212, 0.895)
print func(75, 10.526,0.103, 18.212, 0.895)
print func(100, 10.526,0.103, 18.212, 0.895)
print func(110, 10.526,0.103, 18.212, 0.895)
print func(120, 10.526,0.103, 18.212, 0.895)
print func(150, 10.526,0.103, 18.212, 0.895)
print func(200, 10.526,0.103, 18.212, 0.895)
print func(400, 10.526,0.103, 18.212, 0.895)
print func(500, 10.526,0.103, 18.212, 0.895)


print func(50, 23.32189873,   0.12371408,  18.62556262,   2.30911015)
print func(75, 23.32189873,   0.12371408,  18.62556262,   2.30911015)
print func(100, 23.32189873,   0.12371408,  18.62556262,   2.30911015)
print func(110, 23.32189873,   0.12371408,  18.62556262,   2.30911015)
print func(120, 23.32189873,   0.12371408,  18.62556262,   2.30911015)
print func(150, 23.32189873,   0.12371408,  18.62556262,   2.30911015)
print func(200, 23.32189873,   0.12371408,  18.62556262,   2.30911015)
print func(400, 23.32189873,   0.12371408,  18.62556262,   2.30911015)
print func(505, 23.32189873,   0.12371408,  18.62556262,   2.30911015)

print func(50, 13.39420707,   0.17415179,  22.13290069,   0.16200917)
print func(75, 13.39420707,   0.17415179,  22.13290069,   0.16200917)
print func(100, 13.39420707,   0.17415179,  22.13290069,   0.16200917)
print func(110, 13.39420707,   0.17415179,  22.13290069,   0.16200917)
print func(120, 13.39420707,   0.17415179,  22.13290069,   0.16200917)
print func(150, 13.39420707,   0.17415179,  22.13290069,   0.16200917)
print func(200, 13.39420707,   0.17415179,  22.13290069,   0.16200917)
print func(400, 13.39420707,   0.17415179,  22.13290069,   0.16200917)
print func(500, 13.39420707,   0.17415179,  22.13290069,   0.16200917)



print func(50, 34.40667491,   0.19806837,  22.6177607,    0.05134209)
print func(75, 34.40667491,   0.19806837,  22.6177607,    0.05134209)
print func(100, 34.40667491,   0.19806837,  22.6177607,    0.05134209)
print func(110, 34.40667491,   0.19806837,  22.6177607,    0.05134209)
print func(120, 34.40667491,   0.19806837,  22.6177607,    0.05134209)
print func(150, 34.40667491,   0.19806837,  22.6177607,    0.05134209)
print func(200, 34.40667491,   0.19806837,  22.6177607,    0.05134209)
print func(400, 34.40667491,   0.19806837,  22.6177607,    0.05134209)
print func(500, 34.40667491,   0.19806837,  22.6177607,    0.05134209)

