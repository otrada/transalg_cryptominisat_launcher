from Transalg_runner import runTransalg
from Experiments_launcher import ExperimentLauncher
from Stats import Stats

from Ciphers.Present import Present
from Ciphers.GOST import GOST3412Kuznechik, GOST3412Magma

#k = GOST3412Kuznechik("z" * 32)
#print k.encrypt("z" * 16).encode("hex")
#print k.roundkeys

#k = Present("z" * 10)
#print k.encrypt(["12345678", "90123456"])
#print k.roundkeys

name = "Kuznechik_SB"
runTransalg(32, 1, name)

'''
0x9facd68aa88187bd196e6375bbb2f0ecbccfd5b9b76f2b81ea6ea5f59c69dec8L

0x9facd28aa88187bd196e6375bbb2f0ecbccfd5b9b76f2b81ea6ea5f59c69dec8L 
0x9facd28aa88187bd196e6375bbb2f0ecbccfd5b9b76f2b81ea6ea5f59c6bdec8L 
0x9facd68aa88187bd196e6375bbb2f0ecbccfd5b9b76f2b81ea6ea5f59c6bdec8L 
0x9facd68aa88187bd196e6375bbb2f0ecbccfd5b9b76f2b81ea6ea5f59c69dec8L


0xbc11479a29a86af4feaf2ed0f244de4ddb50f0ab041f43d579d2e2759fcf0900L
rrrrrrrrrrrrrrrrr 0xbc11479a29a86af4feaf2ed0f244de4ddb50f0aa041f43d579d2e2759fcf0900L 0xbc11479a29a86af4feaf2ed0f244de4ddb50f0ab041f43d579d2e2759fcf0900L
4 2 3.0 3.0 1.41421356237

'''


e = ExperimentLauncher(name)
for i in xrange(256 * 5, 99, -1):
    if i < 120:
        n = 10
    if i <= 130:
        n = 20
    if i <= 150:
        n = 50
    elif i <= 200:
        n = 100
    elif i <= 300:
        n = 500
    else:
        n = 1000
    print i
    e.write_statistic(i, 2)
    #e.write_statistic(501, 100)

#s = Stats("Present_128/" + name)
#s.get_file_stat()
#print s.dic

