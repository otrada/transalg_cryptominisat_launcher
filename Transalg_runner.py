import re
import subprocess
#tafile = "Present_80_SB.ta"

def runTransalg(rounds, blocks, ifile="Present_80", t="key"):
    fl = ifile
    ifile = "Ta_files/" + ifile
    tafile = ifile + ".ta"
    print tafile
    ta = open(tafile)
    cont = ta.read()
    cont = re.sub(r'define rounds \d+;', r'define rounds %d;' % rounds, cont)
    cont = re.sub(r'define blocks \d+;', r'define blocks %d;' % blocks, cont)
    ta.close()
    ta = open(tafile, "w")
    ta.write(cont)
    ta.close()
    if t == "key":
        fl = "CNFs/%s.cnf" % fl
    else:
        fl = "CNFs/%s/%d_%d.cnf" % (fl, rounds, blocks),

    #print ifile + "/pres_%d_%d.cnf" % (rounds, blocks)
    p = subprocess.Popen([
        "Transalg/Transalg",
        "-i", tafile,
        "-o", fl,
        "--no-optimize",
        "--verbose"
    ], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    r = p.communicate()
    print r
