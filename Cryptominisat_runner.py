import subprocess
from time import time
from datetime import timedelta


class CryptominisatRunner:
    def __init__(self, filename, verbose=10):
        self.verbose = verbose
        self.filename = filename
        self.vars_number = None
        self.clause_number = None
        self.input_vars_number = None
        self.outut_vars_number = None
        self.output = None
        self.fileReader()
        self.values = []
        self.output_values = []
        self.input_values = []
        self.all_values = []
        self.unsat = False
        self.vars_sublimed = None

    def fileReader(self):
        fl = open("CNFs/%s" % self.filename)
        for i in fl:
            if i[0] == "p":
                s = i.split(" ")
                self.vars_number, self.clause_number = int(s[2]), int(s[3])
            elif i[0] == "c":
                if i.find("input variables") != -1:
                    s = i.split(" ")
                    self.input_vars_number = int(s[-1])
                    break
            else:
                break

    def runCryptominisat(self, output):
        self.proc = subprocess.Popen(["./cryptominisat5",
                                 "--maxsol", "100000000",
                                 "--maxtime", "10000000",
                                 "--verb", "0",
                                 "CNFs/%s" % self.filename],
                                 stdout=subprocess.PIPE)
        solutions = []
        solution = ""
        timestart = time()
        t = ""
        while not self.proc.poll() or t:
            t = self.proc.stdout.readline()
            if t:
                if t == "s SATISFIABLE\n":
                    solution = ""
                if t == "s UNSATISFIABLE\n":
                    yield {
                        "SAT": False,
                        "solution": None,
                        "time": timedelta(seconds=time() - timestart).total_seconds()
                    }

                if t[0] == "v":
                    solution += t
                    if t[-3:] == " 0\n":
                        self.output = solution
                        self.values = []
                        if output["type"] == "out":
                            res = self.presentOutputVars(output["vars_number"],
                                                         output["block_size"])
                        elif output["type"] == "in":
                            res = self.presentInputVars(output["block_size"])
                        else:
                            res = self.presentVars(output["shift"],
                                                   output["shift"] + output["vars_number"],
                                                   output["block_size"])
                        bord = output["border"] if "border" in output else None
                        if res and bord and not (res[bord["shiftl"]: bord["shiftr"]] == bord["content"]):
                            try:
                                self.proc.terminate()
                            except:
                                pass
                            yield {
                                "SAT":      True,
                                "solution": None,
                                "time":     timedelta(seconds=time() - timestart).total_seconds()
                            }
                            break
                        else:
                            yield {
                                "SAT":      True,
                                "solution": res[: output["shift"]],
                                "time":     timedelta(seconds=time() - timestart).total_seconds()
                            }

    def getVarsVector(self):
        v = [-1] * self.vars_number
        s = self.output.split("\n")[:-1]
        for i in s:
            if i[0] != "v":
                break
            else:
                tmp = i[2:].split(" ")[:-1]
                for t in tmp:
                    [sign, t] = [-1, int(t[1:])] if t[0] == "-" else [1, int(t)]
                    v[t-1] = 1 if sign == 1 else 0
        self.values = v
        if self.values[0] == -1:
            self.unsat = True
            return False
        return v

    def getVarsVectorAll(self, start, end):
        if not self.values:
            self.getVarsVector()
        if self.unsat:
            return False
        return self.values[start: end]
    '''
    def getInputVarsVector(self):
        if self.unsat:
            return False
        self.input_values = self.getVarsVectorAll(0, self.input_vars_number)
        return self.input_values

    def getOutputVarsVector(self, n):
        if self.unsat:
            return False
        self.output_values = self.getVarsVectorAll(-n, None)
        return self.output_values
    '''
    def presentVars(self, start, end, wordlen):
        arr = self.values[start: end] if self.values else self.getVarsVectorAll(start, end)
        if self.unsat:
            return False
        self.vars_sublimed = "".join([str(i) for i in self.values])
        self.vars_sublimed = int(self.vars_sublimed, 2)
        res = []
        text = "".join([str(i) for i in arr])
        while len(text) > 0:
            t = text[:wordlen][::-1]
            res.append(int(t, 2))
            text = text[wordlen:]
        return res

    def presentOutputVars(self, n, wordlen):
        return self.presentVars(-n, None, wordlen)

    def presentInputVars(self, wordlen):
        return self.presentVars(0, self.input_vars_number, wordlen)

    def presentSolution(self):
        return self.presentVars(0, None, self.vars_number)


class AllSolutions:
    def __init__(self, filename, right=None):
        self.right_solution = right
        self.filename = filename
        #self.CNFp = DIMACSParser(filename) if not parser else parser
        self.c = None

    def runSolver(self, outp_type, outp_vars, outp_block, shift=0):
        # solutions = []
        # t = 0
        # timestart = time.time()
        # while True:

            self.c = CryptominisatRunner(self.filename, verbose=0)
            return self.c.runCryptominisat(output={
                "type": outp_type,
                "vars_number": outp_vars,
                "block_size": outp_block,
                "shift": shift
            })
            # r = self.c.presentSolution()
            # timecurr = timedelta(seconds=time.time() - timestart)
            # if not r:
            #    print "UNSAT"
            #
            #    print  "time: %d:%d:%d,%d" % (timecurr.seconds // 3600, timecurr.seconds // 60,
            #                                  timecurr.seconds, timecurr.microseconds)
            #    break

            # if r == self.right_solution:
            #     print "RIGHT SOLUTION"
            # print t, [hex(i) for i in solutions[-1]]

            # print  "time: %d:%d:%d,%d" % (timecurr.seconds // 3600, (timecurr.seconds // 60) % 60,
            #                              timecurr.seconds, timecurr.microseconds)
            # t = t + 1
            # self.CNFp.addSolution(self.c.values)
            # print self.c.values
        # return solutions