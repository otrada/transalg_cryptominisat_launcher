# coding: utf-8 -*-
from Transalg_runner import runTransalg
from Experiments_launcher import ExperimentLauncher
from Stats import Stats
import matplotlib.pyplot as plt
from statistics import mean, median
#import numpy as np

#Present_80_SB - 117
#Present_128_SB - 256

name = "AES_128_SB"

#runTransalg(10, 1, name)
'''
e = ExperimentLauncher(name)
num = 1408
for i in xrange(692, 99, -2):
    if i < num * 0.3:
        n = 10
    else:
        n = 20
    #else:
    #    n = 1000
    print i
    #n = 2
    e.write_statistic(i, n)
    #e.write_statistic(501, 100)
'''

# 1408 1.0 0.0 0.084904 0.0329497617897
# 704 1.0 0.0 0.1530515 0.00380494158956
# 422 1.0 0.0 0.1112525 0.00173311872069



'''

p_if_x = []
p_if_times_mean = []
p_if_times_medi = []
p_if_keysn_mean = []
p_if_keysn_medi = []
keysdict = {}

s = Stats("AES_128_SB/AES_128_SB")
s.get_file_stat()
print s.dic
for key in s.dic:
    if key <= BORDER:
        p_if_x.append(key)
        keysdict[key] = {
            "mean":   s.dic[key]["keysn"]["mean"],
            "median": s.dic[key]["keysn"]["median"]}
        p_if_times_mean.append(s.dic[key]["times"]["mean"])
        p_if_times_medi.append(s.dic[key]["times"]["median"])
'''

keysdict = {}

BORDER = 260
# BORDER = 180

p_sb_x = []
p_sb_times_mean = []
p_sb_times_medi = []
p_sb_keysn_mean = []
p_sb_keysn_medi = []


t = Stats("AES_128_SB/AES_128_SB")
t.get_file_stat()
print t.dic
for key in t.dic:
    if key <= BORDER:
        if key in keysdict:
            p_sb_keysn_mean.append(mean([t.dic[key]["keysn"]["mean"], keysdict[key]["mean"]]))
            p_sb_keysn_medi.append(mean([t.dic[key]["keysn"]["median"], keysdict[key]["median"]]))
        else:
            p_sb_keysn_mean.append(t.dic[key]["keysn"]["mean"])
            p_sb_keysn_medi.append(t.dic[key]["keysn"]["median"])
        p_sb_x.append(key)
        p_sb_times_mean.append(t.dic[key]["times"]["mean"])
        p_sb_times_medi.append(t.dic[key]["times"]["median"])

#plt.figure(2, (10, 5))
fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(20, 10), tight_layout=True)

fig.suptitle("AES-128", fontsize=16)
ax1.set_title(u"Время решения")
#ax1.plot(p_if_x, p_if_times_mean, '.', label=u"Среднее для кодировки \nс использованием условных операторов")
#ax1.plot(p_if_x, p_if_times_medi, '.', label=u"Медиана для кодировки \nс использованием условных операторов")
ax1.plot(p_sb_x, p_sb_times_mean, '.', label=u"Среднее для кодировки \nс использованием полиномов Жегалкина")
ax1.plot(p_sb_x, p_sb_times_medi, '.', label=u"Медиана для кодировки \nс использованием полиномов Жегалкина")
ax1.set_xlabel(u"Число известных бит")
ax1.set_ylabel(u"Время решения")
ax1.legend(fontsize=14)

#plt.subplot(2, 2)
ax2.set_title(u"Число ключей")
ax2.plot(p_sb_x, p_sb_keysn_mean, '.', label=u"Среднее")
ax2.plot(p_sb_x, p_sb_keysn_medi, '.', label=u"Медиана")
ax2.legend(fontsize=14)
ax2.set_xlabel(u"Число известных бит")
ax2.set_ylabel(u"Число подходящих ключей")
plt.show()








